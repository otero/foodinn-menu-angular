import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { LayoutMainComponent } from './modules/shared/components/layout-main/layout-main.component';
import { UserGuard } from './modules/core/guards/user.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/products',
    pathMatch: 'full'
  },
  {
    path: '',
    component: LayoutMainComponent,
    children: [
      {
        path: 'products',
        canLoad: [UserGuard],
        loadChildren: () => import('./modules/product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'feedbacks',
        canLoad: [UserGuard],
        loadChildren: () => import('./modules/feedback/feedback.module').then(m => m.FeedbackModule)
      }
    ]
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./modules/admin/admin.module').then(m => m.AdminModule)
  },
  {
    path: 'localization',
    loadChildren: () => import('./modules/localization/localization.module').then(m => m.LocalizationModule)
  },
  {
    path: '**',
    loadChildren: () => import('./modules/page-not-found/page-not-found-routing.module').then(m => m.PageNotFoundRoutingModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
