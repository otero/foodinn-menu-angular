import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './modules/core/services/auth/auth.service';
import { CommonService } from './modules/core/services/common/common.service';
import { LocationService } from './modules/core/services/location/location.service';
import { UserService } from './modules/core/services/user/user.service';
import { environment } from '../environments/environment';


declare var gtag;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, OnDestroy{

  changeLang$;
  form: FormGroup;

  constructor(
    private translateService: TranslateService,
    private authUserService: AuthService,
    private userService: UserService,
    private commonService: CommonService,
    private locationService: LocationService,
    private formBuilder: FormBuilder,

  ) {
    this.initializeTranslation();
  }

  ngOnInit() {
    setTimeout(() => {
      this.oneSignal();
    }, 5000);

    this.changeLang$ = this.commonService.changeLang$.subscribe(lang => {
      this.translateService.use(lang);
    })
  }

  private oneSignal(): void {
    var OneSignal = window['OneSignal'] || [];
    OneSignal.push(["init", {
      appId: environment.onSignalAppId,
      safari_web_id: environment.oneSinalSafariWebId,
      autoRegister: false,
      allowLocalhostAsSecureOrigin: true,
      notifyButton: {
        enable: false
      }
    }]);
    OneSignal.push(() => {
      OneSignal.push(["registerForPushNotifications"])
    });
    OneSignal.push(() => {

      OneSignal.on('subscriptionChange', () =>  {
        OneSignal.getUserId().then((userId) => {
          this.buildForm();
          this.form.patchValue(this.authUserService.loggedUser);
          this.form.get('location').patchValue(this.locationService.loggedLocation.id);
          this.form.get('push_platform_device_id').patchValue(userId);
          this.form.get('roles').patchValue(this.authUserService.loggedUser.roles[0]);
          this.userService.update(this.authUserService.loggedUser.id, this.form.value).subscribe(
            () => {
              console.log('NOTIFICATION SUCESS');
            },
            () => {
              console.log('NOTIFICATION ERROR');
            }
          )
        });
      });
    });

  }

  private initializeTranslation() {
    this.translateService.addLangs(['en', 'es']);
    this.translateService.setDefaultLang(this.commonService.defaultLang);

    let language = this.translateService.getBrowserLang();

    if (!this.translateService.getLangs().find(lang => lang === language)) {
      language = this.translateService.getDefaultLang();
    }

    this.translateService.use(language);
  }

  ngOnDestroy() {
    this.changeLang$.unsubscribe();
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      uuid: null,
      full_name: null,
      lang: null,
      roles: null,
      location: null,
      push_platform_device_id: null
    });
  }
}
