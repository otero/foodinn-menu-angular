import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup} from '@angular/forms';

import { Customer } from '../../../core/models/customer';
import { Product } from '../../../core/models/product';
import { User } from '../../../core/models/user';
import { VariantGroup } from 'src/app/modules/core/models/variant-group';
import { Allergen } from '../../../core/models/allergen';
import { AuthService } from '../../../core/services/auth/auth.service';
import { ProductService } from '../../../core/services/product/product.service';
import { CustomerService } from '../../../core/services/customer/customer.service';
import { AllergenService } from '../../../core/services/allergen/allergen.service';
import { VariantGroupService } from 'src/app/modules/core/services/variant-group/variant-group.service';
import { VariantService } from 'src/app/modules/core/services/variant/variant.service';
import { Variant } from 'src/app/modules/core/models/variant';
import { ProductPriceListService } from 'src/app/modules/core/services/product-price-list/product-price-list.service';
import { ProductAllergenService } from 'src/app/modules/core/services/product-allergen/product-allergen.service';
import { map } from 'rxjs/operators';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { CrossSellService } from 'src/app/modules/core/services/cross-sell/cross-sell.service';
import { CrossSell } from 'src/app/modules/core/models/cross-sell';


@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.css']
})
export class DetailProductComponent implements OnInit, OnDestroy {

  readonly customer: Customer;
  readonly user: User;

  product: Product;
  allergens: Allergen[];
  variantGroups: VariantGroup[] = [];
  variants: Variant[];
  error: boolean;
  form: FormGroup;
  filters: string = 'limit=100000'
  filtersAny: string = `status[eq]=active&${this.filters}`;
  defaultLang: string;
  crossSells: CrossSell[] = [];

  constructor(
    private commonService: CommonService,
    private location: Location,
    private route: ActivatedRoute,
    private productService: ProductService,
    private customerService: CustomerService,
    private authService: AuthService,
    private productAllergenService: ProductAllergenService,
    private allergenService: AllergenService,
    private variantGroupService: VariantGroupService,
    private variantService: VariantService,
    private productPriceListService: ProductPriceListService,
    private salesCenterService: SalesCenterService,
    private crossSellService: CrossSellService
  ) {
    this.customer = this.customerService.loggedCustomer;
    this.user = this.authService.loggedUser;
    this.defaultLang = this.commonService.defaultLang;
    this.variantGroups = [];
  }

  ngOnInit(): void {
    this.commonService.hideAd();
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.fetchProduct(id);
      this.fetchAllergens(id);
      this.fetchVariantGroups(id);
      this.fetchCrossSell(id);
    });
  }

  fetchAllergens(id): void {
    this.productAllergenService.getAll(
      `${this.filters}&product[eq]=${id}`
    )
    .pipe(
      map(productAllergens => {
        let allergens = [];
        productAllergens.forEach((value) => {
            allergens = [...allergens, this.allergenService.map(value.allergen)];
          });
          return allergens;
        }
    ))
    .subscribe(
      allergen => {
        this.allergens = allergen;
      },
      () => {
        this.allergens = null;
      }
    );
  }

  fetchProduct(id): void {
    this.productService.get(id).subscribe(
      async product => {
        this.product = product;
        await this.fetchProductPriceList(product);
      },
     () => {
        this.product = null;
        this.error = true;
      }
    );
  }

  async fetchProductPriceList(product) {
    this.productPriceListService
      .getAll(
        `${this.filtersAny}&product[eq]=${product.id}&priceList[eq]=${this.salesCenterService.loggedSalesCenter.price_list.id}`
      )
      .pipe(map(productsPriceList => {
        return productsPriceList[0]
      }))
      .toPromise()
      .then(productPriceList => {
        this.product.price = productPriceList.main_price;
      })
      .catch(() => {
        this.product.price = 0;
      });
  }

  fetchVariantGroups(product): void {
    this.variantGroups = [];
    this.variantGroupService.getAll(
      `${this.filtersAny}&sort_by=order&order_by=ASC&product[eq]=${product}`
    )
    .subscribe(
      async variantGroups => {
        for (let variantGroup of variantGroups) {
         await this.fetchVariants(variantGroup);
        }
       },
       () => {}
     );
  }

  async fetchVariants(variantGroup: VariantGroup) {
    this.variants = [];
     await this.variantService.getAll(
       `${this.filtersAny}&variantGroup[eq]=${variantGroup.id}&sort_by=order&order_by=ASC`
     )
     .toPromise()
     .then(async variants => {
      this.variants = variants;
      let variantsArray = [];
      for (let variant of this.variants) {
         await this.fetchProductVariant(variant);
         await this.fetchVariantProductPriceList(variant);
         variantsArray.push(variant);
      }
      variantGroup = {...variantGroup, variants: variantsArray}
      this.variantGroups = [...this.variantGroups, variantGroup];

    })
    .catch(() => {
      this.variants = [];
    });

    return this.variants;
  }

  async fetchProductVariant(variant: Variant) {
    await this.productService.get(variant.product.id).toPromise()
      .then(product => {
        variant.product_full = product;
      })
      .catch(() => {
        variant.product_full = null;
      })
  }

  async fetchVariantProductPriceList(variant) {
    await this.productPriceListService.getAll(
      `${this.filtersAny}&product[eq]=${variant.product.id}&priceList[eq]=${this.salesCenterService.loggedSalesCenter.price_list.id}`
    )
    .pipe(map(productsPriceList => {
      return productsPriceList[0]
    }))
    .toPromise()
    .then(productPriceList => {
      variant.product_price_list = productPriceList;
    })
    .catch(() => {
      variant.product_price_list = null;
    });
  }

  async fetchCrossSellProductPriceList(product) {
    await this.productPriceListService.getAll(
      `${this.filtersAny}&product[eq]=${product.id}&priceList[eq]=${this.salesCenterService.loggedSalesCenter.price_list.id}`
    )
    .pipe(map(productsPriceList => {
      return productsPriceList[0]
    }))
    .toPromise()
    .then(productPriceList => {
      product.product_price_list = productPriceList;
    })
    .catch(() => {
      product.product_price_list = null;
    });
  }

  fetchCrossSell(id): void {
    this.crossSellService.getAll(
      `${this.filters}&product[eq]=${id}&visible[eq]=1`
    )
    .subscribe(
      async crossSells => {
        this.crossSells = crossSells;
        for (let variant of this.crossSells) {
            await this.fetchCrossSellProductPriceList(variant.cross_sell_product);
        }
      },
      () => {
        this.crossSells = [];
      });
  }

  variantsById(id) {
    return this.variantGroups.find(variantGroup => variantGroup.id === parseFloat(id)).variants;
  }

  variantById(variants, id) {
    return variants.find(variant => variant.id === parseFloat(id));
  }

  back(event: any): void{
    event.preventDefault();
    this.location.back();
  }

  ngOnDestroy(): void {
    this.commonService.showAd();
  }
}
