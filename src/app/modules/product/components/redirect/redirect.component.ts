import { Component, OnInit } from '@angular/core';

import { TaxonomyService } from '../../../core/services/taxonomy/taxonomy.service';
import { Router } from '@angular/router';
import { Taxonomy } from '../../../core/models/taxonomy';
import { LocationService } from '../../../core/services/location/location.service';
import { Location } from 'src/app/modules/core/models/location';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements OnInit {

  taxonomies: Taxonomy[];
  filtersAny: string = 'status[eq]=active&limit=100000&sort_by=order&order_by=asc&visible[eq]=1';
  location: Location;

  constructor(
    private locationService: LocationService,
    private taxonomyService: TaxonomyService,
    private router: Router
  ) {
    this.location = this.locationService.loggedLocation;
  }

  ngOnInit(): void {
    this.fetchAllTaxonomies();
  }

  fetchAllTaxonomies(): void {
    this.taxonomyService.getAll(`${this.filtersAny}&location[eq]=${this.location.id}&menuRelated[eq]=0`).subscribe(
      taxonomies => {
        this.taxonomies = taxonomies;
        this.router.navigate([
          `/products/taxonomy/${this.taxonomies[0].id}`,
        ]);
      }
    );
  }
}
