import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { TaxonomyService } from '../../../core/services/taxonomy/taxonomy.service';
import { Customer } from '../../../core/models/customer';
import { User } from '../../../core/models/user';
import { Taxonomy } from '../../../core/models/taxonomy';
import { CustomerService } from '../../../core/services/customer/customer.service';
import { AuthService } from '../../../core/services/auth/auth.service';
import { Product } from '../../../core/models/product';
import { ProductTaxonomyService } from '../../../core/services/product-taxonomy/product-taxonomy.service';
import { LocationService } from '../../../core/services/location/location.service';
import { MenuService } from '../../../core/services/menu/menu.service';
import { ProductPriceListService } from 'src/app/modules/core/services/product-price-list/product-price-list.service';
import { Menu } from 'src/app/modules/core/models/menu';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { map } from 'rxjs/operators';
import { ProductTagService } from 'src/app/modules/core/services/product-tag/product-tag.service';
import { WelcomeService } from 'src/app/modules/core/services/welcome/welcome.service';
import { Welcome } from 'src/app/modules/core/models/welcome';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  customer: Customer;
  user: User;
  taxonomies: Taxonomy[];
  taxonomy: Taxonomy;
  products: Product[];
  menus: Menu[];
  nav: Array<any> = [];
  defaultLang: string;
  filters: string = 'limit=100000'
  filtersAny: string = `status[eq]=active&${this.filters}`;
  productNotFound: boolean = false;
  welcome: Welcome;
  showWelcome: boolean;
  vertical = '0px;'

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomerService,
    private taxonomyService: TaxonomyService,
    private locationService: LocationService,
    private authService: AuthService,
    private productTaxonomyService: ProductTaxonomyService,
    private productPriceListService: ProductPriceListService,
    private salesCenterService: SalesCenterService,
    private menuService: MenuService,
    private productTagService: ProductTagService,
    private commonService: CommonService,
    private welcomeService: WelcomeService
  ) {
    this.customer = this.customerService.loggedCustomer;
    this.user = this.authService.loggedUser;
    this.defaultLang = this.commonService.defaultLang;
    this.showWelcome = this.commonService.welcome;
    this.vertical =  `${window.innerHeight-150}px`;
  }

  ngOnInit(): void {
    this.fetchWelcome();
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.fetchTaxonomy(id);
    });

    this.commonService.changeLang$.subscribe(
      lang => {
        this.defaultLang = lang;
      }
    )
  }

  private fetchTaxonomy(id): void {
    this.taxonomyService.get(id).subscribe(
      async taxonomy => {
        this.taxonomy = taxonomy;
        await this.fetchAllTaxonomies();
        await this.fetchMenus();
        this.mergeCategoriesAndMenus(this.taxonomies, this.menus);
        await this.fetchProductByTaxonomy(id);
      },
      () => {}
    )
  }

  private mergeCategoriesAndMenus(categories, menus): void {
    this.nav = [];
    categories.forEach(category => {
      this.nav.push(category);
    });
    menus.forEach(menu => {
      this.nav.push(menu);
    });
    this.nav.sort(function(a, b) {
      return parseFloat(a.order) - parseFloat(b.order);
    });
  }

  async fetchMenus() {
    await this.menuService.getAll(
      `${this.filtersAny}&location[eq]=${this.locationService.loggedLocation.id}&visible[eq]=1`
    ).toPromise()
      .then(menus => {
        this.menus = menus
      })
      .catch(
        () => {
          this.menus = [];
        }
      );
  }

  async fetchAllTaxonomies() {
    await this.taxonomyService.getAll(
      `${this.filtersAny}&location[eq]=${this.locationService.loggedLocation.id}&menuRelated[eq]=0&visible[eq]=1`
    ).toPromise()
      .then(
        taxonomies => {
          this.taxonomies = taxonomies;
        }
      )
      .catch(() => {
        this.taxonomies = [];
      });
  }

  private fetchProductByTaxonomy(id): void {
    this.productTaxonomyService.getAll(
      `${this.filters}&taxonomy[eq]=${id}`
    ).subscribe(
      productsTaxonomies => {
        this.productNotFound = false;
        this.products = [];

        productsTaxonomies.forEach(productTaxonomy => {
          productTaxonomy.product.price = 0;
          this.fetchProductPriceList(productTaxonomy.product);
          this.fetchTags(productTaxonomy.product);
          this.products = [...this.products, productTaxonomy.product];
        });

      },
      () => {
        this.products = [];
        this.productNotFound = true;
      }
    );
  }

  private fetchProductPriceList(product): void {
    this.productPriceListService
      .getAll(
        `${this.filtersAny}&product[eq]=${product.id}&priceList[eq]=${this.salesCenterService.loggedSalesCenter.price_list.id}`
      )
      .pipe(map(productsPriceList => {
        return productsPriceList[0]
      }))
      .subscribe(
        productPriceList => {
          product.price = productPriceList.main_price;
          product.product_price_list = productPriceList;
        },
        () => {}
      );
  }

  async fetchTags(product) {
     await this.productTagService.getAll(
      `${this.filters}&visible[eq]=1&product[eq]=${product.id}`
    )
    .pipe(map(productTags => {
      const tags = [];
      for (let productTag of productTags) {
        if (productTag.tag.status == 'active') {
          tags.push(productTag.tag);
        }
      }

      return tags;
    }))
    .toPromise()
    .then(tags => {
      product.tags = tags;
    })
    .catch(() => {});
  }

  private fetchWelcome(): void {
    this.welcomeService.getAll(
      `${this.filtersAny}&sort_by=id&order_by=DESC&visible[eq]=1`
    )
    .pipe(
      map(welcomes => {
        return welcomes[0];
      })
    )
    .subscribe(
      welcome => {
        this.welcome = welcome;
      },
      () => {
        this.welcome = null;
        this.commonService.hideWelcome();
        this.showWelcome = this.commonService.welcome;
      }
    )
  }

  hideWelcome(): void {
    this.commonService.hideWelcome();
    this.showWelcome = this.commonService.welcome;
  }
}
