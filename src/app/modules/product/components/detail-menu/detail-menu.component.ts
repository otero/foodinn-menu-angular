import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Customer } from 'src/app/modules/core/models/customer';
import { Menu } from 'src/app/modules/core/models/menu';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { CustomerService } from 'src/app/modules/core/services/customer/customer.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { MenuPriceListService } from 'src/app/modules/core/services/menu-price-list/menu-price-list.service';
import { MenuService } from 'src/app/modules/core/services/menu/menu.service';
import { Product } from 'src/app/modules/core/models/product';
import { FormGroup } from '@angular/forms';
import { UtilService } from 'src/app/modules/core/services/util/util.service';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-detail-menu',
  templateUrl: './detail-menu.component.html',
  styleUrls: ['./detail-menu.component.css']
})
export class DetailMenuComponent implements OnInit, OnDestroy {

  readonly customer: Customer;
  readonly user: User;

  menu: Menu;
  error: boolean;
  taxonomies = [];
  products: Product[] = [];
  form: FormGroup;
  filters: string = 'limit=100000'
  filtersAny: string = `status[eq]=active&${this.filters}`;
  defaultLang: string;
  view: any;

  constructor(
    private route: ActivatedRoute,
    private commonService: CommonService,
    private menuService: MenuService,
    private authService: AuthService,
    private customerService: CustomerService,
    private menuPriceListService: MenuPriceListService,
    private salesCenterService: SalesCenterService,
    private utilService: UtilService,
    private router: Router
  ) {
    this.customer = this.customerService.loggedCustomer;
    this.user = this.authService.loggedUser;
    this.defaultLang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.commonService.hideAd();
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.fetchMenu(id);
    });
  }

  fetchMenu(id): void {
    this.menuService.get(id).subscribe(
      async menu => {
        this.menu = menu;
        await this.fetchMenuPriceList(menu);
        await this.utilService
          .getMenuDetailView(
            menu.id,
            this.salesCenterService.loggedSalesCenter.price_list.id,
            this.defaultLang
          )
          .subscribe(response =>{
            this.view = response;
          });
      },
     () => {
        this.menu = null;
        this.error = true;
      }
    );
  }

  async fetchMenuPriceList(menu: Menu) {
    await this.menuPriceListService
      .getAll(
        `${this.filters}&menu[eq]=${menu.id}&priceList[eq]=${this.salesCenterService.loggedSalesCenter.price_list.id}`
      )
      .pipe(map(productsPriceList => {
        return productsPriceList[0]
      }))
      .toPromise()
        .then(menuPriceList => {
          this.menu.price = menuPriceList.price;
        })
        .catch(() => {
          this.menu.price = 0;
        })
  }

  back(event: any): void{
    event.preventDefault();
    this.router.navigate([
      `/products`,
    ]);
  }

  ngOnDestroy(): void {
    this.commonService.showAd();
  }
}
