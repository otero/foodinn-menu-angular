import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { RedirectComponent } from './components/redirect/redirect.component';
import { ProductsComponent } from './components/products/products.component';
import { DetailProductComponent } from './components/detail-product/detail-product.component';
import { DetailMenuComponent } from './components/detail-menu/detail-menu.component';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from '../core/core.module';
import { MaterialModule } from '../material/material.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ RedirectComponent, ProductsComponent, DetailProductComponent, DetailMenuComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    SharedModule,
    CoreModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class ProductModule { }
