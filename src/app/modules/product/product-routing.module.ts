import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailMenuComponent } from './components/detail-menu/detail-menu.component';
import { DetailProductComponent } from './components/detail-product/detail-product.component';
import { ProductsComponent } from './components/products/products.component';
import { RedirectComponent } from './components/redirect/redirect.component';

const routes: Routes = [
  { path: '', component: RedirectComponent },
  { path: 'taxonomy/:id', component: ProductsComponent },
  { path: 'detail/:id', component: DetailProductComponent },
  { path: 'detail-menu/:id', component: DetailMenuComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
