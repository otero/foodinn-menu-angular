import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
  private readonly endpoint: string;

  constructor(
    private router: Router,
    private translateService: TranslateService
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) { }
      }, async (err: any) => {
        if (err instanceof HttpErrorResponse) {
          switch (err.status) {
            case 400:
              console.log(`${this.translateService.instant('ERRORS.BAD_REQUEST')} (${err.status}): ${err.statusText}`);
              break;
            case 401:
              if (request && 
                request.url !== `${environment.apiURL}/version` &&
                request.url !== `${environment.apiURL}/login_check`) {
                location.reload();
              }
              break;
            case 404:
              console.log(`${this.translateService.instant('ERRORS.PRECONDITION_FAILED')} (${err.status}): ${err.statusText}`);
              break;
            case 412:
              console.log(`${this.translateService.instant('ERRORS.PRECONDITION_FAILED')} (${err.status}): ${err.statusText}`);
              break;
            case 500:
              console.log(`${this.translateService.instant('ERRORS.SERVER_ERROR')} (${err.status}): ${err.statusText}`);
              break;
            case 503:
              console.log(`${this.translateService.instant('ERRORS.SERVICE_NOT_AVAILABLE')} (${err.status}): ${err.statusText}`);
              break;
            default:
              console.log(`${this.translateService.instant('ERRORS.GENERIC_MESSAGE')} (${err.status}): ${err.statusText}`);
              break;
          }
        }
      })
    );
  }
}
