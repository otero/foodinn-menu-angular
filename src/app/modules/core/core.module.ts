import {APP_INITIALIZER, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { VersionService } from './services/version/version.service';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorHandlerInterceptor } from './interceptors/error-handler.interceptor';


export function getAPIVersion(versionService: VersionService) {
  return () => versionService.getAPIVersion();
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [HttpClientModule],
  providers: [
    CookieService,
    VersionService,
    {
      provide: APP_INITIALIZER,
      useFactory: getAPIVersion,
      deps: [VersionService],
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true,
    }
  ]
})
export class CoreModule { }
