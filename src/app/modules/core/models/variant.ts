export interface Variant {
  id: number;
    order: number;
    status: string;
    product: any;
    variant_group: any;
    product_full?: any;
    product_price_list?: any;
}
