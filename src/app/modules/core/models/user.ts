export interface User {
  id?: number;
  uuid: string;
  username?: string;
  full_name: string;
  plain_password: string;
  lang: string;
  roles?: any;
  status?: string;
}
