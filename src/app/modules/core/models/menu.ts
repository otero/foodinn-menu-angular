export interface Menu {
  id: number;
  status: string | boolean;
  translations: object;
  visible: boolean;
  order: number;
  type: string;
  price?: number;
}
