export interface Lang {
  id: number;
  name: string;
  iso: string;
  status: string;
}
