export interface ProductPriceList {
  id: number;
  main_price: number;
  variant_price: number;
  price_list: any;
  menu_price: number;
}
