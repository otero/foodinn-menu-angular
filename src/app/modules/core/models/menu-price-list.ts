export interface MenuPriceList {
  id: number;
  price: number;
  price_list: any;
}
