export interface Feedback {
  id: number;
  status: string;
  location: any;
  comment: string;
  rating: number;
  created_at: string;
}
