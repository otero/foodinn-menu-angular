export interface VariantGroup {
  id: number;
  order: number;
  status: string;
  translations: object;
  min_selectable_variants: number;
  max_selectable_variants: number;
  variants: any;
}
