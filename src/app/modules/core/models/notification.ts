export interface Notification {
  id: number;
  visible: boolean;
  send_on_minute: number;
  feedback: boolean;
  external_link: string;
  status: string;
  created_at: string;
  updated_at: string;
  location: any;
  product: any;
  taxonomy: any;
  translations: any;
  new_translations: any;
  current_locale: string;
  default_locale: string;
}
