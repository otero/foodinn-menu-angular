export interface Tag {
  id: number;
  color: string;
  translations: any;
  location: any;
  status: string;
}
