export interface CrossSell {
  id: string;
  visible: boolean;
  product: any;
  cross_sell_product: any;
}
