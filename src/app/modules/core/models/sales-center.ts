export interface SalesCenter {
  id: number;
  status: string;
  translations: any;
  reference: string;
  location: any;
  price_list: any;
  new_translations: any;
  current_locale: string;
  default_locale: string;
}
