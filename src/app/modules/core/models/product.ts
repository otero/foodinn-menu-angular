export interface Product {
  id: number;
  image_url: string;
  status: string | boolean;
  translations: object;
  visible: boolean;
  default_order_on_delivery: string;
  saleable_as_main: boolean;
  saleable_as_variant: boolean;
  parent_product: boolean;
  location: any;
  price?: number;
  product_price_list?: any;
  tag?: any;
  __magic: any;
}
