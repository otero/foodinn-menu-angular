export interface Location {
  id: number;
  reference: string;
  type: string;
  status: string;
  translations: any;
  sales_center: any;
  price_list: any;
  parent_location: Location;
}
