export interface Allergen {
  id: number;
  status: string;
  image_url: string;
  translations: object;
  __magic: any;
}
