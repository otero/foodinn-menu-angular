export interface Welcome {
  id: number;
  status: string;
  visible: boolean;
  image_url: string;
  location: any;
  translations: any;
  default_locale: string;
  current_locale: string;
}
