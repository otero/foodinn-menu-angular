export interface Customer {
  id: number;
  user: any;
  name: string;
  description: string;
  passed_tax: number;
  menu: string;
  cookies_policy: string;
  privacy_policy: string;
  legal_note: string;
  external_or_internal_feedback: string;
  external_feedback_url: string;
  join_us_url: string;
}
