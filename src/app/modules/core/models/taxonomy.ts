export interface Taxonomy {
  id: number;
  location: any;
  order: number;
  visible: boolean;
  menu_related: boolean;
  translations: any;
  promoted: boolean;
  status: string;
  type: string;
  __magic: any;
}
