export interface Ad {
  id: string;
  products_list: any;
  image_url: string;
  visible: boolean;
  show_on_minute: number;
  external_link: string;
  type: string;
  status: string;
  location: any;
  taxonomies_list: any;
  translations: any;
  new_translations: any;
  default_locale: string;
  current_locale: string;
}
