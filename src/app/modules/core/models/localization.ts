export interface Localization {
  name: string;
  reference: string;
  location: number;
  status: boolean;
}
