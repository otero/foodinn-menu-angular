export interface PriceList {
  id: string;
  status: string;
  translations: object;
}
