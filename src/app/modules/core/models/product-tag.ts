export interface ProductTag {
  id: number;
  visible: true;
  status: string;
  product: any;
  tag: any;
}
