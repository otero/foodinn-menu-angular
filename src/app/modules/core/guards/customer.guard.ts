import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth/auth.service';
import { CustomerService } from '../services/customer/customer.service';
import { LocationService } from '../services/location/location.service';
import { UserService } from '../services/user/user.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerGuard implements CanLoad {

  private decodeToken;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private customerService: CustomerService,
    private locationService: LocationService
  ) { }

  async canLoad(
    route: Route,
    segments: UrlSegment[]): Promise<boolean> {
      const role = "ROLE_CUSTOMER";
      if (this.authService.checkToken()) {
        this.decodeToken = JSON.parse(atob(this.authService.getToken().split('.')[1]));

        if (!this.authService.loggedUser) {
          await this.userService.getUser(this.decodeToken.id).then(
            user => {
              this.authService.loggedUser = user;
            }
          )
          .catch(e => {
            this.authService.deleteToken();
            this.router.navigate(['/admin/auth/login']);
            return false;
          });
        }

        if (!this.customerService.loggedCustomer) {
          await this.customerService.get(this.authService.loggedUser.id).then(
            customer => {
              this.customerService.loggedCustomer = customer;
            }
          )
          .catch(() => {
            this.authService.deleteToken();
            this.router.navigate(['/admin/auth/login']);
            return false;
          });
        }

        if (!this.locationService.loggedLocation) {
          await this.locationService.getRestaurantLocationForCustomer().then(
            location => {
              this.locationService.loggedLocation = location;
            }
          ).catch(e => {
              this.authService.deleteToken();
              this.router.navigate(['/admin/auth/login']);
              return false;
            });
        }

        const hasRole = this.authService.loggedUser.roles.find(rol => rol === role);
        if (hasRole === role) {
          return true;
        }
      }

    this.authService.deleteToken();
    this.router.navigate(['/admin/auth/login']);
    return false;
  }
}
