import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { UserService } from '../services/user/user.service';
import { LocationService } from '../services/location/location.service';
import { LocalizationService } from '../services/localization/localization.service';
import { SalesCenterService } from '../services/sales-center/sales-center.service';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanLoad {

  private decodeToken;

  constructor(
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private localizationService: LocalizationService,
    private locationService: LocationService,
    private salesCenterService: SalesCenterService
  ) { }

  canLoad(
    route: Route,
    segments: UrlSegment[]): boolean | Promise<boolean> {
    if (this.authService.checkToken()) {
      this.decodeToken = JSON.parse(atob(this.authService.getToken().split('.')[1]));
      this.userService.setUuid(this.decodeToken.username);
      return this.hydrate();
    }
    return this.reLogin();
  }

  async reLogin(): Promise<boolean> {
    const username = localStorage.getItem('_username');
    await this.authService.login({
      _username: username,
      _password: username
    });
    await this.hydrate();
    return true;
  }

  async hydrate(): Promise<boolean> {
    if (!this.authService.loggedUser) {
      await this.userService.getUser(this.decodeToken.id).then(
        user => {
          this.authService.loggedUser = user;
        },
        async () => {
          this.authService.deleteToken();
          await this.reLogin();
          location.reload();
        }
      );
    }
    if (!this.localizationService.hasLocalization()) {
      await this.router.navigate(['/localization/error']);
      return false;
    }
    if (!this.locationService.loggedLocation) {
      await this.locationService.getLocation(this.localizationService.localization.location)
      .then(location => this.locationService.loggedLocation = location)
      .catch(() => {
        this.router.navigate(['/localization/error']);
        return false;
      });

    }

    if (!this.salesCenterService.loggedSalesCenter) {
      await this.salesCenterService.getSalesCenter(this.localizationService.localization.reference)
      .toPromise()
      .then(salesCenter => this.salesCenterService.loggedSalesCenter = salesCenter)
      .catch(() => {
        this.router.navigate(['/localization/error']);
        return false;
      });

    }

    return true;
  }
}
