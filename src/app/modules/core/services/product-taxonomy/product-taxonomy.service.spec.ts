import { TestBed } from '@angular/core/testing';

import { ProductTaxonomyService } from './product-taxonomy.service';

describe('ProductTaxonomyService', () => {
  let service: ProductTaxonomyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductTaxonomyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
