import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { PriceList } from '../../models/price-list';

@Injectable({
  providedIn: 'root'
})
export class PriceListService {

  private static readonly resource: string = 'price_lists';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${PriceListService.resource}`;
  }

  getAll(querString = null): Observable<PriceList[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let priceLists = [];
          response.data.forEach(priceList => {
            priceLists = [...priceLists, this.map(priceList)];
          });
          return priceLists;
        })
      );
  }

  update(id, priceList): Observable<PriceList> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, priceList).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<PriceList> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(priceList): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), priceList);
  }

  public map(priceList): PriceList {
    return  {
      id: priceList.id,
      status: priceList.status,
      translations: priceList.translations
    };
  }
}
