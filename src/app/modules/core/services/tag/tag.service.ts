import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Tag } from '../../models/tag';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  private static readonly resource: string = 'tags';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${TagService.resource}`;
  }

  getAll(querString = null): Observable<Tag[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let tags = [];
          response.data.forEach(tag => {
            tags = [...tags, this.map(tag)];
          });
          return tags;
        })
      );
  }

  update(id, tag): Observable<Tag> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, tag).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Tag> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(tag): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), tag);
  }

  public map(tag): Tag {
    return  {
      id: tag.id,
      status: tag.status,
      translations: tag.translations,
      location: tag.location,
      color: tag.color
    };
  }
}
