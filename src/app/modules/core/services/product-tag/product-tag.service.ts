import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { ProductTag } from '../../models/product-tag';

@Injectable({
  providedIn: 'root'
})
export class ProductTagService {

  private static readonly resource: string = 'product_tags';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${ProductTagService.resource}`;
  }

  getAll(querString = null): Observable<ProductTag[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let productTags = [];
          response.data.forEach(productTag => {
            productTags = [...productTags, this.map(productTag)];
          });
          return productTags;
        })
      );
  }

  update(id, productTag): Observable<ProductTag> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, productTag).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<ProductTag> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(productTag): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), productTag);
  }

  public map(productTag): ProductTag {
    return  {
      id: productTag.id,
      status: productTag.status,
      visible: productTag.visible,
      product: productTag.product,
      tag: productTag.tag
    };
  }
}
