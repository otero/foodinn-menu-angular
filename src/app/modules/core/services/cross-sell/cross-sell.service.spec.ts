import { TestBed } from '@angular/core/testing';

import { CrossSellService } from './cross-sell.service';

describe('CrossSellService', () => {
  let service: CrossSellService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrossSellService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
