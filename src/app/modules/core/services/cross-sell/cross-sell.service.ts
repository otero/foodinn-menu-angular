import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { CrossSell } from '../../models/cross-sell';

@Injectable({
  providedIn: 'root'
})
export class CrossSellService {

  private static readonly resource: string = 'product_cross_sells';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${CrossSellService.resource}`;
  }

  getAll(querString = null): Observable<CrossSell[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let crossSells = [];
          response.data.forEach(crossSell => {
            crossSells = [...crossSells, this.map(crossSell)];
          });
          return crossSells;
        })
      );
  }

  update(id, crossSell): Observable<CrossSell> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, crossSell).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<CrossSell> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(crossSell): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), crossSell);
  }

  public map(crossSell): CrossSell {
    return  {
      id: crossSell.id,
      visible: crossSell.visible,
      product: crossSell.product,
      cross_sell_product: crossSell.cross_sell_product
    };
  }
}
