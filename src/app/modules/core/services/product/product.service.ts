import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Product } from '../../models/product';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private static readonly resource: string = 'products';

  private productSubject = new Subject<Product>();
  public product$ = this.productSubject.asObservable();
  private productCreateOrUpdateSubject = new Subject<any>();
  public productCreateOrUpdate$ = this.productCreateOrUpdateSubject.asObservable();

  constructor(
    private http: HttpClient,
    private commonService: CommonService,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${ProductService.resource}`;
  }

  getAll(querString = null): Observable<Product[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let products = [];
          response.data.forEach(product => {
            products = [...products, this.map(product)];
          });
          return products;
        })
      );
  }

  update(id, product): Observable<Product> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, product).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Product> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(product): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), product).pipe(
      map(response => {
        return this.map(response.data);
      })
    );;
  }

  sendProduct(product: Product): void {
    this.productSubject.next(product);
  }

  sendProductCreateOrUpdate(id): void {
    this.productCreateOrUpdateSubject.next(id);
  }

  public map(product): Product {
    return  {
      id: product.id,
      status: product.status,
      translations: product.translations,
      image_url: product.image_url,
      visible: product.visible,
      default_order_on_delivery: product.default_order_on_delivery,
      saleable_as_main: product.saleable_as_main,
      saleable_as_variant: product.saleable_as_variant,
      parent_product: (product.parent_product == null) ? false : true,
      location: product.location,
      __magic: product.translations[this.commonService.defaultLang].name
    };
  }
}
