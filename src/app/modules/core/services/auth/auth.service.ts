import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { User } from '../../models/user';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from '../user/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private static readonly resource: string = 'login_check';
  private readonly resourceUrl: string;
  private user: User;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private versionService: VersionService,
    private userService: UserService
  ) {
    this.resourceUrl = `${environment.apiURL}/${AuthService.resource}`;
  }

  get loggedUser(): User {
    return this.user;
  }

  set loggedUser(user: User) {
    this.user = user;
  }

  login(loginDTO: any) {
    const formData = new FormData();
    Object.keys(loginDTO).forEach(key => formData.append(key, loginDTO[key]));

    return this.http.post<any>(this.resourceUrl, formData).pipe(
      map ( response => {
        return this.decryptToken(response.data.token);
      }),
      map ( async token => {
        await this.versionService.getAPIVersion();
        await this.userService.getUser(token.id).then(
          user => {
            this.loggedUser = user;
          }
        );
      })
    ).toPromise();
  }

  decryptToken(token): any {
    const decodedToken = JSON.parse(atob(token.split('.')[1]));
    const expires = new Date((decodedToken.exp * 1000) - 300000);
    this.saveToken(token, expires);
    return decodedToken;
  }
  saveToken(token: string, expires?: number | Date): void {
    this.cookieService.set('foodin-token', token, expires, '/');
  }

  getToken(): string {
    return this.cookieService.get('foodin-token');
  }

  checkToken(): boolean {
    return this.cookieService.check('foodin-token');
  }

  deleteToken(): void {
    this.cookieService.delete('foodin-token', '/');
  }

  checkIfUsernameAndPasswordExist(): boolean {
    const username = localStorage.getItem('_username');

    if (username != null) {
      return true;
    }
    return false;
  }
}
