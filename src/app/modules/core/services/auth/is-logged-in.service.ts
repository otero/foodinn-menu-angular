import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class IsLoggedInService {
  constructor(private auth: AuthService, private router: Router) {}

  resolve(): void {
    if (this.auth.checkToken() === true) {
      this.router.navigate(['/products']);
    }
  }
}
