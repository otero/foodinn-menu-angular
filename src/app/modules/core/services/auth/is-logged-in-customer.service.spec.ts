import { TestBed } from '@angular/core/testing';

import { IsLoggedInCustomerService } from './is-logged-in-customer.service';

describe('IsLoggedInCustomerService', () => {
  let service: IsLoggedInCustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IsLoggedInCustomerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
