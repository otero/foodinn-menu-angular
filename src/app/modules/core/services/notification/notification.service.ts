import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Notification } from '../../models/notification';

import { VersionService } from '../version/version.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private static readonly resource: string = 'notifications';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${NotificationService.resource}`;
  }

  getAll(querString = null): Observable<Notification[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let notifications = [];
          response.data.forEach(notification => {
            notifications = [...notifications, this.map(notification)];
          });
          return notifications;
        })
      );
  }

  update(id, notification): Observable<any> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, notification);
  }

  get(id): Observable<Notification> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(notification): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), notification);
  }

  public map(notification): Notification {
    return  {
      id: notification.id,
      visible: notification.visible,
      send_on_minute: notification.send_on_minute,
      feedback: notification.feedback,
      external_link: notification.external_link,
      status: notification.status,
      created_at: notification.created_at,
      updated_at: notification.updated_at,
      location: notification.location,
      product: notification.product,
      taxonomy: notification.taxonomy,
      translations: notification.translations,
      new_translations: notification.new_translations,
      current_locale: notification.current_locale,
      default_locale: notification.default_locale
    };
  }
}
