import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Taxonomy } from '../../models/taxonomy';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class TaxonomyService {

  private static readonly resource: string = 'taxonomies';

  constructor(
    private http: HttpClient,
    private versionService: VersionService,
    private commonService: CommonService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${TaxonomyService.resource}`;
  }

  getAll(querString = null): Observable<Taxonomy[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let taxonomies = [];
          response.data.forEach(taxonomy => {
            taxonomies = [...taxonomies, this.map(taxonomy)];
          });
          return taxonomies;
        })
      );
  }

  update(id, taxonomy): Observable<Taxonomy> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, taxonomy).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Taxonomy> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(taxonomy): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), taxonomy);
  }

  public map(taxonomy): Taxonomy {
    return  {
      id: taxonomy.id,
      status: taxonomy.status,
      translations: taxonomy.translations,
      visible: taxonomy.visible,
      menu_related: taxonomy.menu_related,
      location: taxonomy.location,
      order: taxonomy.order,
      promoted: taxonomy.promoted,
      type: 'taxonomy',
      __magic:  taxonomy.translations[this.commonService.defaultLang].name
    };
  }
}
