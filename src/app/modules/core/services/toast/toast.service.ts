import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  visibleToast: boolean = true;
  messageTranslate: string;
  color: string;
  private toastSubject = new Subject<any>();
  public toast$ = this.toastSubject.asObservable();

  constructor() { }

  presentToast(messageTranslate: any, verticalPosition = 'bottom') {
    this.toastSubject.next({
      verticalPosition: verticalPosition,
      messageTranslate: messageTranslate
    });
  }
}
