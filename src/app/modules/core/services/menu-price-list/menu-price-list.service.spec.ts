import { TestBed } from '@angular/core/testing';

import { MenuPriceListService } from './menu-price-list.service';

describe('MenuPriceListService', () => {
  let service: MenuPriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MenuPriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
