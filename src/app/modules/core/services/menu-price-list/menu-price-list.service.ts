import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { MenuPriceList } from '../../models/menu-price-list';

@Injectable({
  providedIn: 'root'
})
export class MenuPriceListService {

  private static readonly resource: string = 'menu_price_lists';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${MenuPriceListService.resource}`;
  }

  getAll(querString = null): Observable<MenuPriceList[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let menuPriceLists = [];
          response.data.forEach(menuPriceList => {
            menuPriceLists = [...menuPriceLists, this.map(menuPriceList)];
          });
          return menuPriceLists;
        })
      );
  }

  update(id, menuPriceList): Observable<MenuPriceList> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, menuPriceList).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<MenuPriceList> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(productPriceList): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), productPriceList);
  }

  public map(menuPriceList): MenuPriceList {
    return  {
      id: menuPriceList.id,
      price_list: menuPriceList.price_list,
      price: menuPriceList.price
    };
  }
}
