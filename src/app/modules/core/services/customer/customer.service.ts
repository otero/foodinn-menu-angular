import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Customer } from '../../models/customer';
import { environment } from '../../../../../environments/environment';

import { Observable } from 'rxjs';
import { VersionService } from '../version/version.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  customer: Customer;
  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) { }

  private static readonly resource: string = 'customers';

  set loggedCustomer(customer) {
    this.customer = this.map(customer);
  }

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${CustomerService.resource}`;
  }

  get loggedCustomer(): Customer {
    return this.customer;
  }

  get(user: number): Promise<any> {
    return this.http
      .get<any>(`${this.getResourceUrl()}`).pipe(
        map (response => {
          return this.map(response.data[0]);
        })
      ).toPromise();
  }

  update(id: number, customer: Customer): Observable<any> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, customer).pipe(
      map (response => {
        return this.map(response.data);
      })
    )
  }

  public map(customer): Customer {
    return {
      id: customer.id,
      user: customer.user,
      name: customer.name,
      description: customer.description,
      passed_tax: customer.passed_tax,
      menu: customer.menu,
      cookies_policy: customer.cookies_policy,
      privacy_policy: customer.privacy_policy,
      legal_note: customer.legal_note,
      external_or_internal_feedback: customer.external_or_internal_feedback,
      external_feedback_url: customer.external_feedback_url,
      join_us_url: customer.join_us_url
    };
  }
}
