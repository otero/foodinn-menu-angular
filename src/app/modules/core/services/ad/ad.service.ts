import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Ad } from '../../models/ad';

@Injectable({
  providedIn: 'root'
})
export class AdService {

  private static readonly resource: string = 'comms';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${AdService.resource}`;
  }

  getAll(querString = null): Observable<Ad[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let ads = [];
          response.data.forEach(ad => {
            ads = [...ads, this.map(ad)];
          });
          return ads;
        })
      );
  }

  update(id, ad): Observable<Ad> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, ad).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Ad> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(ad): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), ad);
  }

  public map(ad): Ad {
    return  {
      id: ad.id,
      products_list: ad.products_list,
      image_url: ad.image_url,
      visible: ad.visible,
      show_on_minute: ad.show_on_minute,
      external_link: ad.external_link,
      type: ad.type,
      status: ad.status,
      location: ad.location,
      taxonomies_list: ad.taxonomies_list,
      translations: ad.translations,
      new_translations: ad.new_translations,
      default_locale: ad.default_locale,
      current_locale: ad.current_locale
    };
  }
}
