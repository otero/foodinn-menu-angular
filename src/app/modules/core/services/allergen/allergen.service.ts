import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Allergen } from '../../models/allergen';
import { CommonService } from '../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class AllergenService {

  private static readonly resource: string = 'allergens';

  constructor(
    private http: HttpClient,
    private versionService: VersionService,
    private commonService: CommonService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${AllergenService.resource}`;
  }

  getAll(querString = null): Observable<Allergen[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let allergens = [];
          response.data.forEach(allergen => {
            allergens = [...allergens, this.map(allergen)];
          });
          return allergens;
        })
      );
  }

  update(id, allergen): Observable<Allergen> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, allergen).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Allergen> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(allergen): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), allergen);
  }

  public map(allergen): Allergen {
    return  {
      id: allergen.id,
      status: allergen.status,
      translations: allergen.translations,
      image_url: allergen.image_url,
      __magic:  allergen.translations[this.commonService.defaultLang].name
    };
  }
}
