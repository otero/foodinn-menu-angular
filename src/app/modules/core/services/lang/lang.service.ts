import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { Lang } from '../../models/lang';

import { VersionService } from '../version/version.service';

@Injectable({
  providedIn: 'root'
})
export class LangService {

  private static readonly resource: string = 'langs';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${LangService.resource}`;
  }

  getAll(querString = null): Observable<Lang[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let langs = [];
          response.data.forEach(lang => {
            langs = [...langs, this.map(lang)];
          });
          return langs;
        })
      );
  }

  update(id, lang): Observable<any> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, lang);
  }

  get(id): Observable<Lang> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(lang): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), lang);
  }

  public map(notification): Lang {
    return  {
      id: notification.id,
      name: notification.name,
      iso: notification.iso,
      status: notification.status
    }
  }
}
