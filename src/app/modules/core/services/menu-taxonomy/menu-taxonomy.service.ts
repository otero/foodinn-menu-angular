import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';

@Injectable({
  providedIn: 'root'
})
export class MenuTaxonomyService {

  private static readonly resource: string = 'menu_taxonomies';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${MenuTaxonomyService.resource}`;
  }

  getAll(querString = null): Observable<any[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let menuTaxonomies = [];
          response.data.forEach(menuTaxonomy => {
            menuTaxonomies = [...menuTaxonomies, menuTaxonomy];
          });
          return menuTaxonomies;
        })
      );
  }

  update(id, menuTaxonomy): Observable<any> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, menuTaxonomy).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(product): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), product);
  }
}
