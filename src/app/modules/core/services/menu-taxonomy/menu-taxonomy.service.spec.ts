import { TestBed } from '@angular/core/testing';

import { MenuTaxonomyService } from './menu-taxonomy.service';

describe('MenuTaxonomyService', () => {
  let service: MenuTaxonomyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MenuTaxonomyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
