import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';

import { VersionService } from '../version/version.service';
import { Feedback } from '../../models/feedback';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  private static readonly resource: string = 'feedbacks';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${FeedbackService.resource}`;
  }

  getAll(querString = null): Observable<Feedback[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let feedbacks = [];
          response.data.forEach(feedback => {
            feedbacks = [...feedbacks, this.map(feedback)];
          });
          return feedbacks;
        })
      );
  }

  update(id, feedback): Observable<Feedback> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, feedback).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Feedback> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(feedback): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), feedback);
  }

  public map(feedback): Feedback {
    return  {
      id: feedback.id,
      status: feedback.status,
      comment: feedback.comment,
      rating: feedback.rating,
      location: feedback.location,
      created_at: feedback.created_at
    };
  }
}
