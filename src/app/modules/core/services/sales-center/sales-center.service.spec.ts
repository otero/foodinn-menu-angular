import { TestBed } from '@angular/core/testing';

import { SalesCenterService } from './sales-center.service';

describe('SalesCenterService', () => {
  let service: SalesCenterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalesCenterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
