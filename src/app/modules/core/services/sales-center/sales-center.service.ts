import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { SalesCenter } from '../../models/sales-center';

@Injectable({
  providedIn: 'root'
})
export class SalesCenterService {

  private static readonly resource: string = 'sales_centers';
  loggedSalesCenter: SalesCenter;

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${SalesCenterService.resource}`;
  }

  getAll(querString = null): Observable<SalesCenter[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let salesCenters = [];
          response.data.forEach(salesCenter => {
            salesCenters = [...salesCenters, this.map(salesCenter)];
          });
          return salesCenters;
        })
      );
  }

  update(id, salesCenter): Observable<SalesCenter> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, salesCenter).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<SalesCenter> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(salesCenter): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), salesCenter);
  }

  getSalesCenter(reference): Observable<any> {
    return this.http.get<any>(`${this.getResourceUrl()}?reference[eq]=${reference}`).pipe(
      map(response => {
        const salesCenter =  this.map(response.data[0]);
        this.loggedSalesCenter = salesCenter;

        return salesCenter;
      })
    );
  }

  public map(salesCenter): SalesCenter {
    return  {
      id: salesCenter.id,
      status: salesCenter.status,
      translations: salesCenter.translations,
      reference: salesCenter.reference,
      location: salesCenter.location,
      price_list: salesCenter.price_list,
      new_translations: salesCenter.new_translations,
      current_locale: salesCenter.current_locale,
      default_locale: salesCenter.default_locale
    };
  }
}
