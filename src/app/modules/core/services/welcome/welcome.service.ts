import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Welcome } from '../../models/welcome';

@Injectable({
  providedIn: 'root'
})
export class WelcomeService {

  private static readonly resource: string = 'welcome_splashes';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${WelcomeService.resource}`;
  }

  getAll(querString = null): Observable<Welcome[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let welcomes = [];
          response.data.forEach(welcome => {
            welcomes = [...welcomes, this.map(welcome)];
          });
          return welcomes;
        })
      );
  }

  update(id, welcome): Observable<Welcome> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, welcome).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Welcome> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(welcome): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), welcome);
  }

  public map(welcome): Welcome {
    return  {
      id: welcome.id,
      status: welcome.status,
      visible: welcome.visible,
      translations: welcome.translations,
      location: welcome.location,
      current_locale: welcome.current_locale,
      default_locale: welcome.default_locale,
      image_url: welcome.image_url
    };
  }
}
