import { TestBed } from '@angular/core/testing';

import { ProductPriceListService } from './product-price-list.service';

describe('ProductPriceListService', () => {
  let service: ProductPriceListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductPriceListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
