import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { ProductPriceList } from '../../models/product-price-list';

@Injectable({
  providedIn: 'root'
})
export class ProductPriceListService {

  private static readonly resource: string = 'product_price_lists';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${ProductPriceListService.resource}`;
  }

  getAll(querString = null): Observable<ProductPriceList[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let products = [];
          response.data.forEach(product => {
            products = [...products, this.map(product)];
          });
          return products;
        })
      );
  }

  update(id, productPriceList): Observable<ProductPriceList> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, productPriceList).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<ProductPriceList> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(productPriceList): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), productPriceList);
  }

  public map(product): ProductPriceList {
    return  {
      id: product.id,
      main_price: product.main_price,
      variant_price: product.variant_price,
      price_list: product.price_list,
      menu_price: product.menu_price
    };
  }
}
