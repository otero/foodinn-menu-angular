import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Variant } from '../../models/variant';

@Injectable({
  providedIn: 'root'
})
export class VariantService {

  private static readonly resource: string = 'variants';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${VariantService.resource}`;
  }

  getAll(querString = null): Observable<Variant[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let variants = [];
          response.data.forEach(variant => {
            variants = [...variants, this.map(variant)];
          });
          return variants;
        })
      );
  }

  update(id, variant): Observable<Variant> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, variant).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Variant> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(variant): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), variant);
  }

  public map(variant): Variant {
    return  {
      id: variant.id,
      order: variant.order,
      product: variant.product,
      variant_group: variant.variant_group,
      status: variant.status
    };
  }
}
