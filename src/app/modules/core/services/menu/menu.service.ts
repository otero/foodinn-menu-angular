import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { Menu } from '../../models/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private static readonly resource: string = 'menus';

  private menuSubject = new Subject<Menu>();
  public menu$ = this.menuSubject.asObservable();
  private menuCreateOrUpdateSubject = new Subject<any>();
  public menuCreateOrUpdate$ = this.menuCreateOrUpdateSubject.asObservable();

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${MenuService.resource}`;
  }

  getAll(querString = null): Observable<Menu[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let menus = [];
          response.data.forEach(menu => {
            menus = [...menus, this.map(menu)];
          });
          return menus;
        })
      );
  }

  update(id, menu): Observable<Menu> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, menu).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<Menu> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(menu): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), menu).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  sendMenu(menu: Menu): void {
    this.menuSubject.next(menu);
  }

  sendMenuCreateOrUpdate(id): void {
    this.menuCreateOrUpdateSubject.next(id);
  }

  public map(menu): Menu {
    return  {
      id: menu.id,
      status: menu.status,
      translations: menu.translations,
      visible: menu.visible,
      order: menu.order,
      type: 'menu'
    };
  }
}
