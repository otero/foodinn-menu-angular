import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VersionService } from '../version/version.service';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Location } from '../../models/location';
import { CustomerService } from '../customer/customer.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private static readonly resource: string = 'locations';
  private location: Location;

  constructor(
    private http: HttpClient,
    private versionService: VersionService,
    private customerService: CustomerService
  ) {}

  getResourceUrl(): string {
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${LocationService.resource}`;
  }

  get loggedLocation(): Location {
    return this.location;
  }

  set loggedLocation(location: Location) {
    this.location = location;
  }

  getRestaurantLocationForCustomer() {
    return this.http.get<any>(`${this.getResourceUrl()}?type[eq]=restaurant&status[eq]=active`).pipe(
      map(response => {
        const location = response.data[0];
        this.loggedLocation = location;
        return this.map(location);
      })
    ).toPromise();
  }

  new(location): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), location);
  }

  get(id: string | number) {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}?status[neq]=inactive`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  update(id: string | number, locationDTO) {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, locationDTO).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id: string | number) {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  getLocation(id: string | number) {
    return this.http.get<any>(`${this.getResourceUrl()}?id[eq]=${id}`).pipe(
      map(response => {
        const location = response.data[0];
        this.customerService.loggedCustomer = location.customer;

        return this.map(location);
      })
    ).toPromise();
  }

  private map(location: any): Location {
    return {
      id: location.id,
      reference: location.reference,
      status: location.status,
      type: location.type,
      translations: location.translations,
      sales_center: location.sales_center,
      price_list: location.price_list,
      parent_location: location.parent_location,
    };
  }
}
