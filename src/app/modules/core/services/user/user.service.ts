import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { v4 as uuidv4 } from 'uuid';
import { environment } from 'src/environments/environment';

import { User } from '../../models/user';
import { VersionService } from '../version/version.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private static readonly resource: string = 'users';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${UserService.resource}`;
  }

  getResourceUrlWithOutVersion(): string {
    return `${environment.apiURL}/${UserService.resource}`;
  }

  get(id: string | number): Observable<User> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map (response => {
        const user = response.data;
        return this.map(user);
      }),
      catchError( error => {
        return EMPTY;
      })
    );
  }

  create(user: User): Observable<User> {
    return this.http.post<User>(`${this.getResourceUrlWithOutVersion()}`, user);
  }

  generateUsernameAndPassword(): void{
    const uuid = uuidv4();
    this.setUuid(uuid);
  }

  setUuid(uuid): void{
    localStorage.setItem('_username', uuid);
  }

  getUser(id: string | number) {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map (response => {
        const user = response.data;
        return this.map(user);
      })
    ).toPromise();
  }

  update(id, user): Observable<User> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, user).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  public map(user): User {
    return {
      id: user.id,
      uuid: user.uuid,
      username: user.username,
      full_name: user.full_name,
      plain_password: user.plain_password,
      // lang: user.lang,
      lang: 'es',
      roles: user.roles,
      status: user.status
    };
  }
}
