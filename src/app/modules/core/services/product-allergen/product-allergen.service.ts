import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';

@Injectable({
  providedIn: 'root'
})
export class ProductAllergenService {

  private static readonly resource: string = 'product_allergens';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${ProductAllergenService.resource}`;
  }

  getAll(querString = null): Observable<any[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let productAllergens = [];
          response.data.forEach(productAllergen => {
            productAllergens = [...productAllergens, productAllergen];
          });
          return productAllergens;
        })
      );
  }

  update(id, productAllergen): Observable<any> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, productAllergen).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  get(id): Observable<any> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(product): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), product);
  }
}
