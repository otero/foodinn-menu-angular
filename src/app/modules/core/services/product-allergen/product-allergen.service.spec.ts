import { TestBed } from '@angular/core/testing';

import { ProductAllergenService } from './product-allergen.service';

describe('ProductAllergenService', () => {
  let service: ProductAllergenService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductAllergenService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
