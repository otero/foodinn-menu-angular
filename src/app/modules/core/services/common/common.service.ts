import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private changeLangSubject = new Subject<any>();
  public changeLang$ = this.changeLangSubject.asObservable();

  constructor() { }

  defaultLang: string = 'es';
  welcome: boolean = true;
  ad: boolean = true;

  changeLang(lang): void {
    this.defaultLang = lang;

    this.changeLangSubject.next(lang);
  }

  hideWelcome(): void {
    this.welcome = false;
  }

  hideAd(): void {
    this.ad = false;
  }

  showAd(): void {
    this.ad = true;
  }
}
