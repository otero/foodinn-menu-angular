import { TestBed } from '@angular/core/testing';

import { VariantGroupService } from './variant-group.service';

describe('VariantGroupService', () => {
  let service: VariantGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VariantGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
