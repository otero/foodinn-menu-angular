import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { VersionService } from '../version/version.service';
import { VariantGroup } from '../../models/variant-group';

@Injectable({
  providedIn: 'root'
})
export class VariantGroupService {

  private static readonly resource: string = 'variant_groups';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${VariantGroupService.resource}`;
  }

  getAll(querString = null): Observable<VariantGroup[]> {
    return this.http.
      get<any>(`${this.getResourceUrl()}?${querString}`)
      .pipe(
        map (response => {
          let variantsGroups = [];
          response.data.forEach(variantGroup => {
            variantsGroups = [...variantsGroups, this.map(variantGroup)];
          });
          return variantsGroups;
        })
      );
  }

  update(id, variantGroup): Observable<VariantGroup> {
    return this.http.put<any>(`${this.getResourceUrl()}/${id}`, variantGroup).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  get(id): Observable<VariantGroup> {
    return this.http.get<any>(`${this.getResourceUrl()}/${id}`).pipe(
      map(response => {
        return this.map(response.data);
      })
    );
  }

  delete(id): Observable<any> {
    return this.http.delete<any>(`${this.getResourceUrl()}/${id}`);
  }

  create(variantGroup): Observable<any> {
    return this.http.post<any>(this.getResourceUrl(), variantGroup).pipe(
      map(response => {
        return this.map(response.data);
      })
    );;
  }

  public map(variantGroup): VariantGroup {
    return  {
      id: variantGroup.id,
      order: variantGroup.order,
      status: variantGroup.status,
      translations: variantGroup.translations,
      min_selectable_variants: variantGroup.min_selectable_variants,
      max_selectable_variants: variantGroup.max_selectable_variants,
      variants: variantGroup.variants
    };
  }
}
