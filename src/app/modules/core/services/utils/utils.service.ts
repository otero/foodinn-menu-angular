import { PlatformLocation } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  items: number = 1000000;

  constructor(
    private platformLocation: PlatformLocation
  ) { }

  objectToQueryString(obj) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  generatePathQR() {
    let path = this.platformLocation.protocol + '//' + this.platformLocation.hostname;
    if (this.platformLocation.port) {
      path += ':' + this.platformLocation.port;
    }
    path += '/localization/add/';

    return path;
  }

  objectToQueryStringByFilter(obj) {
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        if (typeof obj[p] == "boolean") {
          if (obj[p]) {
            obj[p] = 1;
          }
          if (!obj[p]) {
            obj[p] = 0;
          }
        }
        const key = p.split('__');
        const value = (key[1] == 'like') ? `%${obj[p]}%` : obj[p];

        if (obj[p] != null) {
          str.push(encodeURIComponent(key[0]) + `[${key[1]}]=` + encodeURIComponent(value));
        }
      }
    return `${str.join("&")}&limit=${this.items}`;
  }

  saveAsImage(parent, saleCenter) {
    const parentElement = parent.qrcElement.nativeElement.querySelector("img").src;
    let blobData = this.convertBase64ToBlob(parentElement);
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blobData, 'Qrcode');
    } else {
      const blob = new Blob([blobData], { type: "image/png" });
      const url = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = url;
      link.download = saleCenter;
      link.click();
    }
  }

  private convertBase64ToBlob(Base64Image: any) {
    const parts = Base64Image.split(';base64,');
    const imageType = parts[0].split(':')[1];
    const decodedData = window.atob(parts[1]);
    const uInt8Array = new Uint8Array(decodedData.length);
    for (let i = 0; i < decodedData.length; ++i) {
      uInt8Array[i] = decodedData.charCodeAt(i);
    }
    return new Blob([uInt8Array], { type: imageType });
  }
}
