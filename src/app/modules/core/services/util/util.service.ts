import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VersionService } from '../version/version.service';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/user';

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  private static readonly resource: string = 'utils';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {
  }
  getResourceUrl(): string {
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${UtilService.resource}`;
  }

  getMenuDetailView(menu, priceList, lang) {
    return this.http.post<any>(`${this.getResourceUrl()}/menu_detail_view`, {
      "menu": menu,
      "price_list": priceList,
      "locale": lang
    }).pipe(
      map(response => {
        return response.data;
      })
    );
  }

  upload(image) {
    return this.http.post<any>(`${this.getResourceUrl()}/upload`, {
      "resource": image
    }, {
        reportProgress: true,
        observe: 'events'
    });
  }
}
