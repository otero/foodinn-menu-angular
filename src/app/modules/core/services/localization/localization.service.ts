import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Localization } from '../../models/localization';

@Injectable({
  providedIn: 'root'
})
export class LocalizationService {
  localization: Localization;

  constructor(private router: Router) {
  }

  generateReference(reference, id, name): void{
    const localization: Localization = {
      name,
      reference: reference,
      location: id,
      status: true
    };
    localStorage.setItem('localization', JSON.stringify(localization));
  }

  get getLocalization(): any {
    return JSON.parse(localStorage.getItem('localization'));
  }

  hasLocalization(): boolean {
    this.localization = this.getLocalization;
    if (this.localization == null) {
      return false;
    }
    return true;
  }

  changeStatus(status): void {
    this.localization.status = status;
    localStorage.setItem('localization', JSON.stringify(this.localization));
  }

  delete(): void {
    this.localization = null;
    localStorage.removeItem('localization');
  }

  resolve(): void {
    if (!this.hasLocalization()) {
      this.router.navigate(['/localization/error']);
    }
  }
}
