import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { VersionService } from '../version/version.service';

@Injectable({
  providedIn: 'root'
})
export class ResourcesService {

  private static readonly resource: string = 'resources';

  constructor(
    private http: HttpClient,
    private versionService: VersionService
  ) {}

  getResourceUrl(): string{
    return `${environment.apiURL}/${this.versionService.currentAPIVersion}/${ResourcesService.resource}`;
  }

  create(resource: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'multipart/form-data');
    return this.http.post<any>(`${this.getResourceUrl()}`, resource, {
        reportProgress: true,
        observe: 'events',
        headers: headers
    });
  }
}
