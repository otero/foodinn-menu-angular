import { Injectable } from '@angular/core';

import { Translation } from '../../models/translation';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  constructor() { }

  static map(translation) {
    let object = {};
    Object.keys(translation).forEach(key => {
      let keyTranslation = [];
      let trans: Translation = {
        name: translation[key].name,
        description: translation[key].description
      };
      keyTranslation[key] = trans;
      
      Object.assign(object, keyTranslation)
    });

    return object;
  }
}
