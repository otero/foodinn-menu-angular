import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageNotFoundRoutingModule } from './page-not-found-routing.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { LocationComponent } from './components/location/location.component';
import { LocalizationComponent } from './components/localization/localization.component';


@NgModule({
  declarations: [PageNotFoundComponent, LocationComponent, LocalizationComponent],
  imports: [
    CommonModule,
    PageNotFoundRoutingModule
  ]
})
export class PageNotFoundModule { }
