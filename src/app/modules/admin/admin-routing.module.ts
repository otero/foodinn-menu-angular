import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutAdminMainComponent } from '../shared/components/layout-admin-main/layout-admin-main.component';
import { LayoutAdminAuthComponent } from '../shared/components/layout-admin-auth/layout-admin-auth.component';
import { CustomerGuard } from '../core/guards/customer.guard';

const routes: Routes = [
  {
    path: '',
    component: LayoutAdminMainComponent,
    children: [
      {
        path: '',
        redirectTo: '/admin/home',
        pathMatch: 'full'
      },
      {
        path: 'menu',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/menu/menu.module').then(m => m.MenuModule)
      },
      {
        path: 'setting',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/setting/setting.module').then(m => m.SettingModule)
      },
      {
        path: 'notification',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/notification/notification.module').then(m => m.NotificationModule)
      },
      {
        path: 'lang',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/language/language.module').then(m => m.LanguageModule)
      },
      {
        path: 'price-list',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/price-list/price-list.module').then(m => m.PriceListModule)
      },
      {
        path: 'sales-center',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/sales-center/sales-center.module').then(m => m.SalesCenterModule)
      },
      {
        path: 'welcome',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/welcome/welcome.module').then(m => m.WelcomeModule)
      },
      {
        path: 'tag',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/tag/tag.module').then(m => m.TagModule)
      },
      {
        path: 'ad',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/ad/ad.module').then(m => m.AdModule)
      },
      {
        path: 'cross-sell',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/cross-sell/cross-sell.module').then(m => m.CrossSellModule)
      },
      {
        path: 'taxonomy',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/taxonomy/taxonomy.module').then(m => m.TaxonomyModule)
      },
      {
        path: 'product',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'home',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'feedback',
        canLoad: [CustomerGuard],
        loadChildren: () => import('./modules/feedback/feedback.module').then(m => m.FeedbackModule)
      }
    ]
  },
  {
    path: '',
    component: LayoutAdminAuthComponent,
    children: [
      {
        path: 'auth',
        loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)
      }
    ]
  },
  {
    path: '**',
    loadChildren: () => import('./modules/page-not-found/page-not-found-routing.module').then(m => m.PageNotFoundRoutingModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
