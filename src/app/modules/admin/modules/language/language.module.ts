import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LanguageRoutingModule } from './language-routing.module';
import { CoreModule } from 'src/app/modules/core/core.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ListComponent } from './components/list/list.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { SearchComponent } from './components/search/search.component';
import { FormComponent } from './components/form/form.component';


@NgModule({
  declarations: [ListComponent, SearchComponent, FormComponent],
  imports: [
    CommonModule,
    LanguageRoutingModule,
    CoreModule,
    SharedModule,
    ReactiveFormsModule,
    MaterialModule
  ]
})
export class LanguageModule { }
