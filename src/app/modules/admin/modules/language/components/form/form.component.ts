import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  langId: string | number;
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  lang: Lang;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private langService: LangService,
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.langId = params.id;
      this.initPage(this.langId);
    });
  }

  private initPage(id: string | number): void {
    if (id) {
      this.texts = this.textEdit;
      this.fetchLang(id);
      this.isNew = false;
      return;
    }

    this.texts = this.textNew;
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const langDTO = this.form.value;

      if (this.langId === undefined) {
        this.createLang(langDTO);
        return;
      }
      this.updateLang(langDTO);
    }
  }

  private createLang(DTO): void {
    this.langService.create(DTO).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.LANG.CREATE_SUCCESS');
        this.router.navigate(['/admin/lang']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateLang(DTO): void {
    this.langService.update(this.langId, DTO).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.LANG.UPDATE_SUCCESS');
        this.router.navigate(['/admin/lang']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private fetchLang(id): void {
    this.langService.get(id).subscribe(
      lang => {
        this.lang = lang;
        this.form.patchValue(this.lang);
      },
      () => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/lang']);
      }
    );
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      name: [null, [Validators.required]],
      iso: [null, [Validators.required]]
    });
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.LANGS.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.LANGS.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
