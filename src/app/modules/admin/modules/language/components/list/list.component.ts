import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Lang } from 'src/app/modules/core/models/lang';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  langs: Lang[] = [];
  displayedColumns: string[] = ['name', 'iso', 'status'];
  dataSource: MatTableDataSource<Lang>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private langService: LangService,
    private toastService: ToastService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.fetchLangs(this.filters);
  }

  private fetchLangs(queryString): void {
    this.langService.getAll(queryString).subscribe(
      langs => {
        this.langs = langs;
        this.dataSource = new MatTableDataSource<Lang>(this.langs);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.langs = [];
        this.dataSource = new MatTableDataSource<Lang>(this.langs);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.langService.delete(this.deleteId).subscribe(
      () => {
        this.fetchLangs(this.filters);
        this.toastService.presentToast('ADMIN.LANGS.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  setFilter(event): void {
    this.filters = event;
    this.fetchLangs(this.filters);
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.LANGS.DELETE_TITLE',
        subtitle: 'ADMIN.LANGS.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }
}
