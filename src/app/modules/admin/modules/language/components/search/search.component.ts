import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UtilsService } from 'src/app/modules/core/services/utils/utils.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  form: FormGroup;

  @Output() emit = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private utilService: UtilsService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  doFilter(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const query = this.utilService.objectToQueryStringByFilter(this.form.value);
      this.emit.emit(query);
      this.form.reset();
    }
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      name__like: [],
      iso__like: [],
      status__eq: 'active'
    });
  }
}
