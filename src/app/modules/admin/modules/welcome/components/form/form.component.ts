import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { User } from 'src/app/modules/core/models/user';
import { Welcome } from 'src/app/modules/core/models/welcome';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { WelcomeService } from 'src/app/modules/core/services/welcome/welcome.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  welcomeId: string | number;
  fileType: string[] = ['image/jpeg', 'image/png'];
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  welcome: Welcome;
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  translationsKeys: string[] = [];
  authUser: User;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private welcomeService: WelcomeService,
    private locationService: LocationService,
    private authService: AuthService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.buildForm();
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.welcomeId = params.id;
      this.initPage(this.welcomeId);
    });
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;
        this.addLangs();
      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          title: ['', [Validators.required]],
          description: ['', [Validators.required]],
        }),
      );
    }
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;

      this.isNew = false;

      await this.fetchLangs();
      await this.fetchWelcome(id);
      return;
    }

    this.texts = this.textNew;
    await this.fetchLangs();
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const welcomeDTO = this.form.value;

      if (this.welcomeId === undefined) {
        this.createWelcome(welcomeDTO);
        return;
      }
      this.updateWelcome(welcomeDTO);
    }
  }

  private createWelcome(DTO): void {
    this.welcomeService.create(this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.WELCOME.CREATE_SUCCESS');
        this.router.navigate(['/admin/welcome']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateWelcome(DTO): void {
    this.welcomeService.update(this.welcomeId, this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.WELCOME.UPDATE_SUCCESS');
        this.router.navigate(['/admin/welcome']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }

    return DTO;
  }

  async fetchWelcome(id) {
    await this.welcomeService.get(id).toPromise()
      .then(welcome => {
        this.welcome = welcome;
        this.form.patchValue(this.welcome);
        this.form.get('location').patchValue(this.welcome.location.id);
      })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/welcome']);
      });
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  setFile(file): void {
    this.form.get('image_url').patchValue(file.file);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      image_url: [null, [Validators.required]],
      visible: [true, [Validators.required]],
      translations: this.formBuilder.group({})
    });
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.WELCOME.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.WELCOME.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
