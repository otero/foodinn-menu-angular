import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { User } from 'src/app/modules/core/models/user';
import { Welcome } from 'src/app/modules/core/models/welcome';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { WelcomeService } from 'src/app/modules/core/services/welcome/welcome.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  welcomes: Welcome[] = [];
  displayedColumns: string[] = ['name', 'description', 'visible', 'actions'];
  dataSource: MatTableDataSource<Welcome>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private welcomeService: WelcomeService,
    private locationService: LocationService,
    private toastService: ToastService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchWelcomes(this.filters);
  }

  private fetchWelcomes(queryString): void {
    this.welcomeService.getAll(queryString).subscribe(
      welcomes => {
        this.welcomes = welcomes;
        this.dataSource = new MatTableDataSource<Welcome>(this.welcomes);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.welcomes = [];
        this.dataSource = new MatTableDataSource<Welcome>(this.welcomes);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.welcomeService.delete(this.deleteId).subscribe(
      () => {
        this.fetchWelcomes(this.filters);
        this.toastService.presentToast('ADMIN.WELCOME.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchWelcomes(this.filters);
  }

  changeVisible(id, visible) {
    const welcome = this.welcomes.find(taxonomy => taxonomy.id === id);
    welcome.visible = visible ? false : true;
    const form = this.buildForm(welcome);
    this.welcomeService.update(welcome.id, form.value).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.WELCOME.UPDATE_SUCCESS');
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    )
    return welcome.visible;
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.WELCOME.DELETE_TITLE',
        subtitle: 'ADMIN.WELCOME.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }

  private buildForm(welcome) {
    const form = this.formBuilder.group({
      translations: this.formBuilder.group({}),
      location: this.locationService.loggedLocation.id,
      visible: welcome.visible,
      image_url: welcome.image_url
    });
    Object.keys(welcome.translations).forEach(key => {
      this.addTranslation(form, key, welcome.translations[key]);
    });
    return form;
  }

  private addTranslation(form, lang, value): void {
    const translation = form.get('translations') as FormGroup;
    translation.addControl(
      lang,
      this.formBuilder.group({
        title: [value.title, [Validators.required]],
        description: [value.description, [Validators.required]],
      }),
    );
  }

  search(event: Event) {
    this.dataSource.filterPredicate = (data, filter: string)  => {
      const accumulator = (currentTerm, key) => {
        return key === 'translations' ? currentTerm + data.translations[this.lang].title : currentTerm + data[key];
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase()
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
