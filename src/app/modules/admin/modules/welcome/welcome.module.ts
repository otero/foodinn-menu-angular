import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomeRoutingModule } from './welcome-routing.module';
import { ListComponent } from './components/list/list.component';
import { SearchComponent } from './components/search/search.component';
import { FormComponent } from './components/form/form.component';
import { CoreModule } from 'src/app/modules/core/core.module';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ListComponent, SearchComponent, FormComponent],
  imports: [
    CommonModule,
    WelcomeRoutingModule,
    CoreModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class WelcomeModule { }
