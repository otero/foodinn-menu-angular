import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Location } from 'src/app/modules/core/models/location';
import { PriceList } from 'src/app/modules/core/models/price-list';
import { Product } from 'src/app/modules/core/models/product';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { PriceListService } from 'src/app/modules/core/services/price-list/price-list.service';
import { ProductPriceListService } from 'src/app/modules/core/services/product-price-list/product-price-list.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';

@Component({
  selector: 'app-product-price-list',
  templateUrl: './product-price-list.component.html',
  styleUrls: ['./product-price-list.component.css']
})
export class ProductPriceListComponent implements OnInit, OnDestroy {

  @Input() parentForm: FormGroup;
  @Input() isNew: boolean;

  user: User;
  location: Location;
  priceLists: PriceList[] = [];
  filtersAny: string = 'status[eq]=active&limit=100000';
  product: Product;
  product$;
  productCreateOrUpdate$;

  constructor(
    private authService: AuthService,
    private locationService: LocationService,
    private formBuilder: FormBuilder,
    private priceListService: PriceListService,
    private productService: ProductService,
    private productPriceListService: ProductPriceListService
  ) { }

  async ngOnInit() {
    this.location = this.locationService.loggedLocation;
    this.user = this.authService.loggedUser;
    await this.fetchPriceLists();
    await this.callObservables();

    if (this.isNew) {
      await this.createEmptyProductPriceList();
    }
  }

  private callObservables(): void {
    this.product$ = this.productService.product$.subscribe(
      product => {
        if (product !== null) {
          this.product = product;
          this.updateProductPriceListFormByProduct(product);
        }
      },
      () => {}
    );

    this.productCreateOrUpdate$ = this.productService.productCreateOrUpdate$.subscribe(
      product => {
        if (product !== null) {
          this.createOrUpdateProductPriceList(product, this.parentForm.get('productPriceList').value);
        }
      },
      () => {}
    );
  }

  private createOrUpdateProductPriceList(product, productPriceLists): void {
    productPriceLists.forEach(productPriceList => {
      const productPriceListDTO = this.mapDTOProductPriceList(product, productPriceList);
      if (productPriceList.id === undefined || productPriceList.id === null) {
        this.productPriceListService.create(productPriceListDTO).subscribe(
          () => {},
          () => {}
        );
        return;
      }
      this.productPriceListService.update(productPriceList.id, productPriceListDTO).subscribe(
        () => {},
        () => {}
      );
    });
  }

  private createEmptyProductPriceList(): void {
    this.priceLists.forEach(priceList => {
      this.productPriceListField.push(this.createOrUpdateProductPriceListForm(
        null,
        0,
        0,
        0,
        priceList.id,
        priceList.translations[this.user.lang].name
      ));
    });
  }

  async updateProductPriceListFormByProduct(product) {
    for (const priceList of this.priceLists) {
      await this.productPriceListService
        .getAll(`${this.filtersAny}&product[eq]=${product.id}&priceList[eq]=${priceList.id}`)
        .pipe(map(products => {
          return products[0]
        }))
        .toPromise()
        .then(productPriceList => {
          this.productPriceListField.push(this.createOrUpdateProductPriceListForm(
            productPriceList.id,
            productPriceList.main_price,
            productPriceList.variant_price,
            productPriceList.menu_price,
            productPriceList.price_list.id,
            productPriceList.price_list.translations[this.user.lang].name
          ));
        })
        .catch(
          () =>{
            this.productPriceListField.push(this.createOrUpdateProductPriceListForm(
              null,
              0,
              0,
              0,
              priceList.id,
              priceList.translations[this.user.lang].name
            ));
          }
        );
    }
  }

  async fetchPriceLists() {
    await this.priceListService.getAll(`${this.filtersAny}&location[eq]=${this.location.id}`).toPromise()
      .then(
        priceLists => {
          this.priceLists = priceLists;
        }
      )
  }

  private createOrUpdateProductPriceListForm(id, main_price, variant_price, menu_price, priceListId, priceListName): FormGroup {
    const form = this.formBuilder.group({
      id: id,
      price_list: priceListId,
      main_price: [main_price, [Validators.required, Validators.pattern("[0-9]+(\.[0-9][0-9]?)?")]],
      variant_price: [variant_price, [Validators.required, Validators.pattern("[0-9]+(\.[0-9][0-9]?)?")]],
      menu_price: [menu_price, [Validators.required, Validators.pattern("[0-9]+(\.[0-9][0-9]?)?")]],
      price_list_name: priceListName,
    });

    return form;
  }

  get productPriceListField(): FormArray {
    return this.parentForm.get('productPriceList') as FormArray;
  }

  private mapDTOProductPriceList(productId, productPriceList): any {
    let productPriceListDTO = {...productPriceList};
    productPriceListDTO = {...productPriceListDTO, product: parseFloat(productId)}
    delete productPriceListDTO.id;
    delete productPriceListDTO.price_list_name;

    return productPriceListDTO;
  }

  ngOnDestroy(): void {
    this.productCreateOrUpdate$.unsubscribe();
    this.product$.unsubscribe();
  }
}
