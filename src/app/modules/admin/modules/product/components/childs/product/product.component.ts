import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Location } from 'src/app/modules/core/models/location';
import { PriceList } from 'src/app/modules/core/models/price-list';
import { Product } from 'src/app/modules/core/models/product';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { PriceListService } from 'src/app/modules/core/services/price-list/price-list.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { ProductPriceListService } from 'src/app/modules/core/services/product-price-list/product-price-list.service';
import { map } from 'rxjs/operators';
import { Lang } from 'src/app/modules/core/models/lang';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';

@Component({
  selector: 'app-product-child',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit, OnDestroy {

  @Input() parentForm: FormGroup;
  user: User;
  filtersAny: string = 'status[eq]=active&limit=100000';
  location: Location;
  priceLists: PriceList[] = [];
  translationsKeys: string[] = [];
  product: Product;
  product$;
  langs: Lang[] = [];
  productCreateOrUpdate$;

  constructor(
    private authService: AuthService,
    private locationService: LocationService,
    private formBuilder: FormBuilder,
    private priceListService: PriceListService,
    private langService: LangService,
    private productService: ProductService,
    private productPriceListService: ProductPriceListService
  ) {
    this.location = this.locationService.loggedLocation;
    this.user = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.fetchLangs();
    this.fetchPriceLists();
    this.callObservables();
  }

  private callObservables(): void {
    this.product$ = this.productService.product$.subscribe(
      product => {
        if (product !== null) {
          this.product = product;

          this.fetchSalesFormat(product);
        }
      },
      () => {}
    );

    this.productCreateOrUpdate$ = this.productService.productCreateOrUpdate$.subscribe(
      product => {
        if (product !== null) {
          this.createOrUpdateSalesFormat(product, this.parentForm.get('salesFormat').value);
        }
      },
      () => {}
    );
  }

  private fetchSalesFormat(product: Product): void {
    this.productService.getAll(`${this.filtersAny}&parentProduct[eq]=${product.id}`).subscribe(
      salesFormat => {
        salesFormat.forEach(saleFormat => {
          this.patchFormSalesFormat(saleFormat);
        });
      },
      () => {}
    )
  }

  async patchFormSalesFormat(salesFormat) {
    const saleFormatPriceList = await this.updateSalesFormatPriceListFormBySaleFormat(salesFormat.id);
    const form = this.formBuilder.group({
      id: salesFormat.id,
      location: this.location.id,
      visible: salesFormat.visible,
      translations: this.formBuilder.group({}),
      saleFormatPriceList: this.formBuilder.array(saleFormatPriceList),
      saleable_as_main: [salesFormat.saleable_as_main,[Validators.required]],
      saleable_as_variant: [salesFormat.saleable_as_variant, [Validators.required]],
    });

    this.translationsKeys = [];
    for (let lang of this.langs) {
      this.addLangs(form, lang, salesFormat);
    }

    this.salesFormatField.push(form);
    return form;
  }

  private fetchPriceLists(): void {
    this.priceListService.getAll(
      `${this.filtersAny}&location[eq]=${this.location.id}&sort_by=id&order_by=desc`
    ).subscribe(
      priceLists => {
        this.priceLists = priceLists;
      }
    )
  }

  addSalesFormat(): void {
    this.salesFormatField.push(this.createSalesFormat());
  }

  private createSalesFormat(): FormGroup {
    const form = this.formBuilder.group({
      id: null,
      translations: this.formBuilder.group({}),
      location: this.location.id,
      visible: true,
      saleFormatPriceList: this.formBuilder.array(this.createEmptySalesFormatPriceList()),
      saleable_as_main: [false,[Validators.required]],
      saleable_as_variant: [false, [Validators.required]],
    });

    this.translationsKeys = [];
    for (let lang of this.langs) {
      this.addLangs(form, lang);
    }

    return form;
  }

  private createEmptySalesFormatPriceList(): FormGroup[] {
    const salesFormatPriceListCollection = [];
    this.priceLists.forEach(priceList => {
      salesFormatPriceListCollection.push(this.createOrUpdateSaleFormatPriceListForm(
        null,
        0,
        0,
        priceList.id,
        priceList.translations[this.user.lang].name,
        0
      ));
    });

    return salesFormatPriceListCollection;
  }

  private createOrUpdateSalesFormat(product, salesFormat): void {
    salesFormat.forEach(saleFormat => {
    const saleFormatDTO = this.mapDTOSalesFormat(product, saleFormat);
     if (saleFormat.id === undefined || saleFormat.id === null) {
      this.productService.create(saleFormatDTO).subscribe(
        product => {
          this.createOrUpdateSalesFormatPriceList(product.id, saleFormat.saleFormatPriceList);
        }
      );
       return;
     }

     this.productService.update(saleFormat.id, saleFormatDTO).subscribe(
      product => {
        this.createOrUpdateSalesFormatPriceList(product.id, saleFormat.saleFormatPriceList);
       }
     );
    });
  }

  createOrUpdateSalesFormatPriceList(productId, saleFormatPriceLists): void {
    saleFormatPriceLists.forEach(saleFormatPriceList => {
      const salesFormatPriceListDTO = this.mapDTOSalesFormatPriceList(productId, saleFormatPriceList);
      if (saleFormatPriceList.id === undefined || saleFormatPriceList.id === null) {
        this.productPriceListService.create(salesFormatPriceListDTO).subscribe(
          () => {},
          () => {}
        );
        return;
      }
      this.productPriceListService.update(saleFormatPriceList.id, salesFormatPriceListDTO).subscribe(
        () => {},
        () => {}
      );
    });
  }

  async updateSalesFormatPriceListFormBySaleFormat(saleFormatId) {
    const salesFormatPriceListCollection = [];
    for (const priceList of this.priceLists) {
      await this.productPriceListService
        .getAll(`${this.filtersAny}&product[eq]=${saleFormatId}&priceList[eq]=${priceList.id}`)
        .pipe(map(products => {
          return products[0]
        }))
      .toPromise()
        .then(saleFormatPriceList => {
          salesFormatPriceListCollection.push(this.createOrUpdateSaleFormatPriceListForm(
            saleFormatPriceList.id,
            saleFormatPriceList.main_price,
            saleFormatPriceList.variant_price,
            saleFormatPriceList.price_list.id,
            saleFormatPriceList.price_list.translations[this.user.lang].name,
            saleFormatPriceList.menu_price
          ));
        })
        .catch(
          () =>{
            salesFormatPriceListCollection.push(this.createOrUpdateSaleFormatPriceListForm(
              null,
              0,
              0,
              priceList.id,
              priceList.translations[this.user.lang].name,
              0
            ));
          }
        );
    }
    return salesFormatPriceListCollection;
  }

  private createOrUpdateSaleFormatPriceListForm(id, mainPrice, variantPrice,  priceListId, priceListName, menuPrice): FormGroup {
    const form = this.formBuilder.group({
      id: id,
      price_list: priceListId,
      main_price: [mainPrice, [Validators.required, Validators.pattern("[0-9]+(\.[0-9][0-9]?)?")]],
      variant_price: [variantPrice, [Validators.required, Validators.pattern("[0-9]+(\.[0-9][0-9]?)?")]],
      price_list_name: priceListName,
      menu_price: menuPrice
    });

    return form;
  }

  salesFormatPriceListField(saleFormat): FormArray {
    return this.salesFormatField.controls[saleFormat].get('saleFormatPriceList') as FormArray;
  }

  removeSalesFormat(index, idSaleFormat): void {
    this.salesFormatField.removeAt(index);
    if (idSaleFormat === null) {
      return;
    }
    this.productService.delete(idSaleFormat).subscribe(
      () => {},
      () => {}
    );
  }

  private mapDTOSalesFormat(product, saleFormat) {
    let saleFormatDTO = {...saleFormat};
    saleFormatDTO = {...saleFormat, parent_product: parseFloat(product)};
    saleFormatDTO = {...saleFormatDTO, image_url: this.parentForm.get('image_url').value};
    saleFormatDTO = {...saleFormatDTO, default_order_on_delivery: this.parentForm.get('default_order_on_delivery').value};
    delete saleFormatDTO.id;
    delete saleFormatDTO.saleFormatPriceList;

    return saleFormatDTO;
  }

  private mapDTOSalesFormatPriceList(productId, saleFormatPriceList): any {
    let saleFormatPriceListDTO = {...saleFormatPriceList};
    saleFormatPriceListDTO = {...saleFormatPriceList, product: parseFloat(productId)}
    delete saleFormatPriceListDTO.id;
    delete saleFormatPriceListDTO.price_list_name;

    return saleFormatPriceListDTO;
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;

      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(form, lang, saleFormat = null): void {
    this.translationsKeys.push(lang.iso);

    const name = (saleFormat == null) ? null : saleFormat.translations[lang.iso].name;
    const description = (saleFormat == null) ? null : saleFormat.translations[lang.iso].description;

    form.get('translations').addControl(
      lang.iso,
      this.formBuilder.group({
        name: [name, [Validators.required]],
        description: [description, [Validators.required]],
      }),
    );
  }

  get salesFormatField(): FormArray {
    return this.parentForm.get('salesFormat') as FormArray;
  }

  getTransControl(index, key) {
    return this.parentForm['controls']['salesFormat']['controls'][index]['controls']['translations']['controls'][key];
  }

  ngOnDestroy() {
    this.product$.unsubscribe();
    this.productCreateOrUpdate$.unsubscribe();
  }
}
