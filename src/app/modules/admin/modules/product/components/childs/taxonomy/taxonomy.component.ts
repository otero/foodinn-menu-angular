import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';

import { Location } from 'src/app/modules/core/models/location';
import { Product } from 'src/app/modules/core/models/product';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ProductTaxonomyService } from 'src/app/modules/core/services/product-taxonomy/product-taxonomy.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';

@Component({
  selector: 'app-product-taxonomy',
  templateUrl: './taxonomy.component.html',
  styleUrls: ['./taxonomy.component.css']
})
export class TaxonomyComponent implements OnInit, OnDestroy {

  @Input() parentForm: FormGroup;

  taxonomies: Taxonomy[] = [];
  product$;
  filtersAny: string = 'status[eq]=active&limit=100000';
  filtersWithOutStatus: string = 'limit=100000'
  defaultLang: string;
  productCreateOrUpdate$;
  product: Product;
  location: Location;
  user: User;

  constructor(
    private locationService: LocationService,
    private taxonomyService: TaxonomyService,
    private authService: AuthService,
    private productService: ProductService,
    private productTaxonomyService: ProductTaxonomyService,
    private commonService: CommonService
  ) {
    this.location = this.locationService.loggedLocation;
    this.user = this.authService.loggedUser;
    this.defaultLang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchTaxonomies();
    this.callObservables();
  }

  private callObservables(): void {
    this.product$ = this.productService.product$.subscribe(
      product => {
        if (product !== null) {
          this.product = product;
          this.fetchTaxonomyByProduct(product);
        }
      },
      () => {}
    );

    this.productCreateOrUpdate$ = this.productService.productCreateOrUpdate$.subscribe(
      product => {
        if (product !== null) {
          this.createOrUpdateProductTaxonomies(product, this.parentForm.get('taxonomies').value);
        }
      },
      () => {}
    );
  }

  private fetchTaxonomyByProduct(product): void {
    this.productTaxonomyService.getAll(`${this.filtersWithOutStatus}&product[eq]=${product.id}`)
      .pipe(
        map(taxonomies => {
          const array = [];
          taxonomies.forEach(taxonomy => {
            array.push(taxonomy.taxonomy.id);
          });
          return array;
        })
      )
      .subscribe(
        taxonomies => {
          this.taxonomiesControl.patchValue(taxonomies);
        },
        () => this.taxonomiesControl.patchValue([])
      );
  }

  private fetchTaxonomies(): void {
    this.taxonomyService.getAll(`${this.filtersAny}&visible[eq]=1`).subscribe(
      taxonomies => {
        this.taxonomies = taxonomies;
      },
      () => {
        this.taxonomies = [];
      }
    )
  }

  async createOrUpdateProductTaxonomies(product, taxonomies) {
    await this.deleteAllProductTaxonomies(product);
    await taxonomies.forEach(taxonomy => {
      this.productTaxonomyService.create({
        taxonomy: parseInt(taxonomy),
        product: parseInt(product),
        order: 0
      }).subscribe(
        () => {},
        () => {}
      );
    });
  }

  async deleteAllProductTaxonomies(product) {
    if (product === null) {
      return;
    }
    await this.productTaxonomyService
      .getAll(`${this.filtersWithOutStatus}&product[eq]=${product}`).toPromise()
      .then(async productTaxonomies => {
        const deletePromises = [];
        productTaxonomies.forEach(productTaxonomy => {
          let promise = this.productTaxonomyService.delete(productTaxonomy.id).toPromise();
          deletePromises.push(promise);
        });
        await Promise.all(deletePromises);
      })
      .catch(() => {});
  }

  get taxonomiesControl() {
    return this.parentForm.controls['taxonomies'];
  }

  ngOnDestroy() {
    this.productCreateOrUpdate$.unsubscribe();
    this.product$.unsubscribe();
  }
}
