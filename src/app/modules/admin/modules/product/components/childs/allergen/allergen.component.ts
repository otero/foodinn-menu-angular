import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Allergen } from 'src/app/modules/core/models/allergen';

import { Location } from 'src/app/modules/core/models/location';
import { Product } from 'src/app/modules/core/models/product';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { User } from 'src/app/modules/core/models/user';
import { AllergenService } from 'src/app/modules/core/services/allergen/allergen.service';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ProductAllergenService } from 'src/app/modules/core/services/product-allergen/product-allergen.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';

@Component({
  selector: 'app-product-allergen',
  templateUrl: './allergen.component.html',
  styleUrls: ['./allergen.component.css']
})
export class AllergenComponent implements OnInit, OnDestroy {

  @Input() parentForm: FormGroup;

  allergens: Allergen[] = [];
  product$;
  filtersAny: string = 'status[eq]=active&limit=100000';
  filtersWithOutStatus: string = 'limit=100000'
  defaultLang: string;
  productCreateOrUpdate$;
  product: Product;
  location: Location;
  user: User;

  constructor(
    private locationService: LocationService,
    private allergenService: AllergenService,
    private authService: AuthService,
    private productService: ProductService,
    private productAllergenService: ProductAllergenService,
    private commonService: CommonService
  ) {
    this.location = this.locationService.loggedLocation;
    this.user = this.authService.loggedUser;
    this.defaultLang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchAllergens();
    this.callObservables();

    this.parentForm.get('allergens').statusChanges.subscribe(
      data => {
        console.log(data)
      }
    )
    this.parentForm.get('allergens').valueChanges.subscribe(
      data => {
        console.log(data)
      }
    );
  }

  private callObservables(): void {
    this.product$ = this.productService.product$.subscribe(
      product => {
        if (product !== null) {
          this.product = product;
          this.fetchAllergenByProduct(product);
        }
      },
      () => {}
    );

    this.productCreateOrUpdate$ = this.productService.productCreateOrUpdate$.subscribe(
      product => {
        if (product !== null) {
          this.createOrUpdateProductAllergen(product, this.parentForm.get('allergens').value);
        }
      },
      () => {}
    );
  }

  private fetchAllergenByProduct(product): void {
    this.productAllergenService.getAll(`${this.filtersWithOutStatus}&product[eq]=${product.id}`)
      .pipe(
        map(allergens => {
          const array = [];
          allergens.forEach(allergen => {
            array.push(allergen.allergen.id);
          });
          return array;
        })
      )
      .subscribe(
        allergens => {
          this.allergensControl.patchValue(allergens);
        },
        () => this.allergensControl.patchValue([])
      );
  }

  private fetchAllergens(): void {
    this.allergenService.getAll(`${this.filtersAny}`).subscribe(
      allergens => {
        this.allergens = allergens;
      },
      () => {
        this.allergens = [];
      }
    )
  }

  async createOrUpdateProductAllergen(product, allergens) {
    await this.deleteAllProductAllergens(product);
    await allergens.forEach(allergen => {
      this.productAllergenService.create({
        allergen: parseInt(allergen),
        product: parseInt(product)
      }).subscribe(
        () => {},
        () => {}
      );
    });
  }

  async deleteAllProductAllergens(product) {
    if (product === null) {
      return;
    }
    await this.productAllergenService
      .getAll(`${this.filtersWithOutStatus}&product[eq]=${product}`).toPromise()
      .then(async productAllergens => {
        const deletePromises = [];
        productAllergens.forEach(productAllergen => {
          let promise = this.productAllergenService.delete(productAllergen.id).toPromise();
          deletePromises.push(promise);
        });
        await Promise.all(deletePromises);
      })
      .catch(() => {});
  }

  get allergensControl() {
    return this.parentForm.controls['allergens'];
  }

  ngOnDestroy() {
    this.productCreateOrUpdate$.unsubscribe();
    this.product$.unsubscribe();
  }
}
