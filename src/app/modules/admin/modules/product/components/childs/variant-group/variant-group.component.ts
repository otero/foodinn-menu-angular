import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Lang } from 'src/app/modules/core/models/lang';
import { Location } from 'src/app/modules/core/models/location';
import { Product } from 'src/app/modules/core/models/product';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { VariantGroupService } from 'src/app/modules/core/services/variant-group/variant-group.service';
import { VariantService } from 'src/app/modules/core/services/variant/variant.service';

@Component({
  selector: 'app-product-variant-group',
  templateUrl: './variant-group.component.html',
  styleUrls: ['./variant-group.component.css']
})
export class VariantGroupComponent implements OnInit, OnDestroy {

  @Input() parentForm: FormGroup;
  user: User;
  location: Location;
  product: Product;
  variants: Product[] = [];
  selectedVariants = [];
  filtersAny: string = 'status[eq]=active&limit=100000';
  translationsKeys: string[] = [];
  langs: Lang[] = [];
  product$;
  productCreateOrUpdate$;

  constructor(
    private authService: AuthService,
    private locationService: LocationService,
    private formBuilder: FormBuilder,
    private productService: ProductService,
    private variantGroupService: VariantGroupService,
    private variantService: VariantService,
    private langService: LangService,
  ) {
    this.location = this.locationService.loggedLocation;
    this.user = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.fetchLangs();
    this.fetchVariants();
    this.callObservables();
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;

      })
      .catch(() => {
        this.langs = [];
      });
  }

  private callObservables(): void {
    this.product$ = this.productService.product$.subscribe(
      product => {
        if (product !== null) {
          this.product = product;
          this.fetchVariantGroups(this.product);
        }
      },
      () => {}
    );

    this.productCreateOrUpdate$ = this.productService.productCreateOrUpdate$.subscribe(
      product => {
        if (product !== null) {
          this.createOrUpdateGroupVariants(product, this.parentForm.get('groupVariants').value);
        }
      },
      () => {}
    );
  }

  private fetchVariantGroups(product): void {
    this.variantGroupService.getAll(`
    ${this.filtersAny}&product[eq]=${product.id}&sort_by=order&order_by=ASC`).subscribe(
      variantGroups => {
        variantGroups.forEach(variantGroup => {
          let formGroupVariant = this.patchFormVariantGroups(variantGroup);
          this.variantService.getAll(
            `${this.filtersAny}&variantGroup[eq]=${variantGroup.id}&sort_by=order&order_by=ASC`
          )
          .pipe(
            map(variants => {
              const array = [];
              variants.forEach(variant => {
                array.push(variant.product.id);
              });
              return array;
            })
          )
          .subscribe(
            variants => {
              formGroupVariant.controls['variants'].patchValue(variants);
            },
            () => {}
          );
        });
      },
      () => {}
    );
  }

  private patchFormVariantGroups(variantGroup): FormGroup {
    const form = this.formBuilder.group({
      id: variantGroup.id,
      min_selectable_variants: [variantGroup.min_selectable_variants, [Validators.required]],
      max_selectable_variants: [variantGroup.max_selectable_variants, [Validators.required]],
      order: variantGroup.order,
      translations: this.formBuilder.group({}),
      variants: [[], [Validators.required]],
    });

    this.translationsKeys = [];
    for (let lang of this.langs) {
      this.addLangs(form, lang, variantGroup);
    }

    this.groupVariantsField.push(form);
    this.selectedVariants.push(this.variants);
    return form;
  }

  private addLangs(form, lang, variantGroup = null): void {
    this.translationsKeys.push(lang.iso);

    const name = (variantGroup == null) ? null : variantGroup.translations[lang.iso].name;
    const description = (variantGroup == null) ? null : variantGroup.translations[lang.iso].description;

    form.get('translations').addControl(
      lang.iso,
      this.formBuilder.group({
        name: [name, [Validators.required]],
        description: [description, [Validators.required]],
      }),
    );
  }

  createOrUpdateGroupVariants(productId, groupVariants): void {
    groupVariants.forEach(groupVariant => {
     const groupVariantDTO = this.mapDTOVariantGroup(productId, groupVariant);
     if (groupVariant.id === undefined || groupVariant.id === null) {
      this.variantGroupService.create(groupVariantDTO).subscribe(
        groupVariantResponse => {
          this.createOrUpdateVariants(groupVariantResponse.id, groupVariant.variants);
        }
      );
      return;
     }
     this.variantGroupService.update(groupVariant.id, groupVariantDTO).subscribe(
      groupVariantResponse => {
        this.createOrUpdateVariants(groupVariantResponse.id, groupVariant.variants);
       }
     );
    });
  }

  async createOrUpdateVariants(variantGroupId, variants) {
    await this.deleteAllVariantsByGroupVariant(variantGroupId);
    await variants.forEach(variant => {
      this.variantService.create({
        product: parseInt(variant),
        variant_group: parseInt(variantGroupId),
        order: 0
      }).subscribe(
        () => {},
        () => {}
      )
    });
  }

  async deleteAllVariantsByGroupVariant(variantGroup) {
    await this.variantService
      .getAll(
        `${this.filtersAny}&variantGroup[eq]=${variantGroup}&sort_by=order&order_by=ASC`
      ).toPromise()
      .then(async variants => {
        const deletePromises = [];
        variants.forEach(variant => {
          let promise = this.variantService.delete(variant.id).toPromise();
          deletePromises.push(promise);
        });
        await Promise.all(deletePromises);
      })
      .catch(() => {});
  }

  addGroupVariants(): void {
    this.groupVariantsField.push(this.createGroupVariants());
    this.selectedVariants.push(this.variants);
  }

  removeGroupVariants(index, idGroupVariant): void {
    this.groupVariantsField.removeAt(index);
    this.selectedVariants = this.removeItemOnce(this.selectedVariants, index);
    if (idGroupVariant === null) {
      return;
    }
    this.variantGroupService.delete(idGroupVariant).subscribe(
      () => {},
      () => {}
    );
  }

  private removeItemOnce(array, value) {
    array.splice(value, 1);
    return array;
  }

  private createGroupVariants(): FormGroup {
    const form = this.formBuilder.group({
      id: null,
      order: 0,
      min_selectable_variants: [0, [Validators.required, Validators.min(0)]],
      max_selectable_variants: [1, [Validators.required, Validators.min(1)]],
      translations: this.formBuilder.group({}),
      variants: [[], [Validators.required]],
    });

    this.translationsKeys = [];
    for (let lang of this.langs) {
      this.addLangs(form, lang);
    }

    return form;
  }

  private fetchVariants(): void {
    this.productService.getAll(
      `${this.filtersAny}&saleableAsVariant[eq]=1`
    ).subscribe(
      products => {
        this.variants = products;
      },
      () => {
        this.variants = [];
      }
    );
  }

  get groupVariantsField(): FormArray {
    return this.parentForm.get('groupVariants') as FormArray;
  }

  private mapDTOVariantGroup(productId, variantGroup): any {
    let variantGroupDTO = {...variantGroup};
    variantGroupDTO = {...variantGroup, product: parseFloat(productId)}
    variantGroupDTO.min_selectable_variants = parseFloat(variantGroupDTO.min_selectable_variants);
    variantGroupDTO.max_selectable_variants = parseFloat(variantGroupDTO.max_selectable_variants);
    variantGroupDTO.order = parseFloat(variantGroupDTO.order);
    delete variantGroupDTO.id;
    delete variantGroupDTO.variants;

    return variantGroupDTO;
  }

  getTransControl(index, key) {
    return this.parentForm['controls']['groupVariants']['controls'][index]['controls']['translations']['controls'][key];
  }

  getGroupVariantControl(index) {
    const control = this.parentForm['controls']['groupVariants']['controls'][index] as FormControl;
    return control;
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  search(value, variantGroup) {
    let filter = value.toLowerCase();
    this.selectedVariants[variantGroup] = this.variants.filter(option => option.translations[this.user.lang].name.toLowerCase().startsWith(filter));
  }

  ngOnDestroy(): void {
    this.productCreateOrUpdate$.unsubscribe();
    this.product$.unsubscribe();
  }
}
