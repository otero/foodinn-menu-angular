import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { Product } from 'src/app/modules/core/models/product';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  productId: string | number;
  fileType: string[] = ['image/jpeg', 'image/png'];
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  product: Product;
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  translationsKeys: string[] = [];
  authUser: User;
  defaultLang: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private locationService: LocationService,
    private authService: AuthService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private productService: ProductService,
    private commonService: CommonService
  ) {
    this.buildForm();
    this.defaultLang = this.commonService.defaultLang;
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.productId = params.id;
      this.initPage(this.productId);
    });
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;
        this.addLangs();
      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          name: ['', [Validators.required]],
          description: ['', [Validators.required]],
        }),
      );
    }
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;

      this.isNew = false;

      await this.fetchLangs();
      await this.fetchProduct(id);
      return;
    }

    this.texts = this.textNew;
    await this.fetchLangs();
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const productDTO = this.form.value;

      if (this.productId === undefined) {
        this.createProduct(productDTO);
        return;
      }
      this.updateProduct(productDTO);
    }
  }

  private createProduct(DTO): void {
    this.productService.create(this.mapDTO(DTO)).subscribe(
      async product => {
        const productId = product.id;
        this.toastService.presentToast('ADMIN.PRODUCT.CREATE_SUCCESS');
        this.productService.sendProductCreateOrUpdate(productId);
        this.router.navigate(['/admin/product']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateProduct(DTO): void {
    this.productService.update(this.productId, this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.PRODUCT.UPDATE_SUCCESS');
        this.productService.sendProductCreateOrUpdate(this.productId);
        this.router.navigate(['/admin/product']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }
    delete DTO.taxonomies;
    delete DTO.allergens;
    delete DTO.groupVariants;
    delete DTO.salesFormat;
    delete DTO.productPriceList;
    delete DTO.parent_product;
    return DTO;
  }

  async fetchProduct(id) {
    await this.productService.get(id).toPromise()
      .then(async product => {
        this.product = product;
        this.form.patchValue(this.product);
        this.form.get('location').patchValue(this.product.location.id);

        setTimeout(() => {
          this.productService.sendProduct(this.product);
        }, 200);

       })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/product']);
      });
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  setFile(file): void {
    this.form.get('image_url').patchValue(file.file);
  }


  private buildForm(): void {
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      visible: [true, [Validators.required]],
      image_url: [null, [Validators.required]],
      saleable_as_variant: [false, Validators.required],
      saleable_as_main: [false, Validators.required],
      default_order_on_delivery: ['starter', [Validators.required]],
      translations: this.formBuilder.group({}),
      taxonomies: [[], [Validators.required]],
      allergens: [[], []],
      groupVariants: this.formBuilder.array([]),
      salesFormat: this.formBuilder.array([]),
      productPriceList: this.formBuilder.array([]),
      parent_product: false
    });
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.PRODUCT.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.PRODUCT.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
