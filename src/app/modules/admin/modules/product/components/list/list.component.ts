import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';
import { Product } from 'src/app/modules/core/models/product';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';

import { User } from 'src/app/modules/core/models/user';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { MenuService } from 'src/app/modules/core/services/menu/menu.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  products: Product[] = [];
  displayedColumns: string[] = ['name', 'visible', 'actions'];
  dataSource: MatTableDataSource<Product>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;
  menus: number = 0;
  product: number = 0;
  taxonomies: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private productService: ProductService,
    private toastService: ToastService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private locationService: LocationService,
    private taxonomyService: TaxonomyService,
    private menuService: MenuService
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchProducts(this.filters);
    this.fetchTaxonomies();
    this.fetchMenus();
    this.fetchProduct();
  }

  private fetchTaxonomies(): void {
    this.taxonomyService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(taxonomies => {
      this.taxonomies = taxonomies
    });
  }

  private fetchMenus(): void {
    this.menuService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(menus => {
      this.menus = menus;
    });
  }

  private fetchProduct(): void {
    this.productService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(product => {
      this.product = product;
    });
  }

  private fetchProducts(queryString): void {
    this.productService.getAll(queryString).subscribe(
      products => {
        this.products = products;
        this.dataSource = new MatTableDataSource<Product>(this.products);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.products = [];
        this.dataSource = new MatTableDataSource<Product>(this.products);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.productService.delete(this.deleteId).subscribe(
      () => {
        this.fetchProducts(this.filters);
        this.toastService.presentToast('ADMIN.PRODUCT.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  changeVisible(id, visible) {
    const product = this.products.find(product => product.id === id);
    product.visible = visible ? false : true;
    const form = this.buildForm(product);
    this.productService.update(product.id, this.mapDTO(form.value)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.PRODUCT.UPDATE_SUCCESS');
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    )
    return product.visible;
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchProducts(this.filters);
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.PRODUCT.DELETE_TITLE',
        subtitle: 'ADMIN.PRODUCT.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }

  private buildForm(product) {
    const form = this.formBuilder.group({
      translations: this.formBuilder.group({}),
      location: this.locationService.loggedLocation.id,
      visible: product.visible,
      image_url: product.image_url,
      saleable_as_main: product.saleable_as_main,
      saleable_as_variant: product.saleable_as_variant,
      default_order_on_delivery: product.default_order_on_delivery,
    });

    Object.keys(product.translations).forEach(key => {
      this.addTranslation(form, key, product.translations[key]);
    });

    return form;
  }

  private addTranslation(form, lang, value): void {
    const translation = form.get('translations') as FormGroup;
    translation.addControl(
      lang,
      this.formBuilder.group({
        name: [value.name, [Validators.required]],
        description: [value.description, [Validators.required]]
      }),
    );
  }

  search(event: Event) {
    this.dataSource.filterPredicate = (data, filter: string)  => {
      const accumulator = (currentTerm, key) => {
        return key === 'translations' ? currentTerm + data.translations[this.lang].name : currentTerm + data[key];
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase()
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private mapDTO(form) {
    const DTO = { ...form }

    return DTO;
  }
}
