import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Lang } from 'src/app/modules/core/models/lang';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { UtilsService } from 'src/app/modules/core/services/utils/utils.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  langs: Lang[] = [];
  filtersLang: string = 'status[eq]=active&limit=100000';
  lang: string = null;

  @Output() emit = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private langService: LangService,
    private utilService: UtilsService
  ) {
    this.buildForm();

    this.form.get('lang').valueChanges.subscribe(result => {
      if (result != this.lang && result != null) {
        this.emit.emit({
          isLang: true,
          lang: result
        });
        this.lang = result;
      }
    });
  }

  ngOnInit(): void {
    this.fetchLangs();
  }

  doFilter(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const query = this.utilService.objectToQueryStringByFilter(this.mapDTO(this.form.value));
      this.emit.emit({
        isLang: false,
        query: query
      });
      this.form.reset();
    }
  }

  private fetchLangs(): void {
    this.langService.getAll(this.filtersLang).subscribe(
      langs => {
        this.langs = langs;
      },
      () => {
        this.langs = [];
      }
    )
  }

  private mapDTO(form) {
    const DTO = { ...form }
    delete DTO.lang;

    return DTO;
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      status__eq: 'active',
      visible__eq: null,
      saleable_as_main__eq: null,
      saleable_as_variant__eq: null,
      lang: []
    });
  }
}
