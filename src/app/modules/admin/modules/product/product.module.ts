import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgSelectModule } from '@ng-select/ng-select';

import { ProductRoutingModule } from './product-routing.module';
import { ListComponent } from './components/list/list.component';
import { SearchComponent } from './components/search/search.component';
import { FormComponent } from './components/form/form.component';
import { CoreModule } from 'src/app/modules/core/core.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TaxonomyComponent } from './components/childs/taxonomy/taxonomy.component';
import { AllergenComponent } from './components/childs/allergen/allergen.component';
import { ProductPriceListComponent } from './components/childs/product-price-list/product-price-list.component';
import { ProductComponent } from './components/childs/product/product.component';
import { VariantGroupComponent } from './components/childs/variant-group/variant-group.component';


@NgModule({
  declarations: [ListComponent, SearchComponent, FormComponent, TaxonomyComponent, AllergenComponent, ProductPriceListComponent, ProductComponent, VariantGroupComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    CoreModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class ProductModule { }
