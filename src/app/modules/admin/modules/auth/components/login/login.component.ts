import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  error = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  auth(event): void {
    event.preventDefault();
    if (this.form.valid) {
      this.authService.login(this.form.value)
      .then(
        () => {
          this.router.navigate(['admin/home']);
        }
      )
      .catch(() => {
        this.error = true;
        this.authService.deleteToken();
      });
    }
  }

  private buildForm(): void{
    this.form = this.formBuilder.group({
      _username: ['', [Validators.required]],
      _password: ['', [Validators.required]]
    });
  }

}
