import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuRoutingModule } from './menu-routing.module';
import { ListComponent } from './components/list/list.component';
import { SearchComponent } from './components/search/search.component';
import { FormComponent } from './components/form/form.component';
import { CoreModule } from 'src/app/modules/core/core.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TaxonomyComponent } from './components/childs/taxonomy/taxonomy.component';
import { MenuPriceListComponent } from './components/childs/menu-price-list/menu-price-list.component';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [ListComponent, SearchComponent, FormComponent, TaxonomyComponent, MenuPriceListComponent],
  imports: [
    CommonModule,
    MenuRoutingModule,
    CoreModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    NgSelectModule
  ]
})
export class MenuModule { }
