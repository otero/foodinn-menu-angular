import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { Product } from 'src/app/modules/core/models/product';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { Menu } from 'src/app/modules/core/models/menu';
import { MenuService } from 'src/app/modules/core/services/menu/menu.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  menuId: string | number;
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  menu: Menu;
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  translationsKeys: string[] = [];
  authUser: User;
  defaultLang: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private locationService: LocationService,
    private authService: AuthService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private menuService: MenuService,
    private commonService: CommonService
  ) {
    this.buildForm();
    this.defaultLang = this.commonService.defaultLang;
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.menuId = params.id;
      this.initPage(this.menuId);
    });
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;
        this.addLangs();
      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          name: ['', [Validators.required]],
          description: ['', [Validators.required]],
        }),
      );
    }
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;

      this.isNew = false;

      await this.fetchLangs();
      await this.fetchMenu(id);
      return;
    }

    this.texts = this.textNew;
    await this.fetchLangs();
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const menuDTO = this.form.value;

      if (this.menuId === undefined) {
        this.createMenu(menuDTO);
        return;
      }
      this.updateMenu(menuDTO);
    }
  }

  private createMenu(DTO): void {
    this.menuService.create(this.mapDTO(DTO)).subscribe(
      async menu => {
        const menuId = menu.id;
        this.toastService.presentToast('ADMIN.MENU.CREATE_SUCCESS');
        this.menuService.sendMenuCreateOrUpdate(menuId);
        this.router.navigate(['/admin/menu']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateMenu(DTO): void {
    this.menuService.update(this.menuId, this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.MENU.UPDATE_SUCCESS');
        this.menuService.sendMenuCreateOrUpdate(this.menuId);
        this.router.navigate(['/admin/menu']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }
    DTO.order = parseFloat(DTO.order);
    delete DTO.taxonomies;
    delete DTO.salesFormat;
    return DTO;
  }

  async fetchMenu(id) {
    await this.menuService.get(id).toPromise()
      .then(async menu => {
        this.menu = menu;
        this.form.patchValue(this.menu);

        setTimeout(() => {
          this.menuService.sendMenu(this.menu);
        }, 200);

       })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/menu']);
      });
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      visible: [true, [Validators.required]],
      translations: this.formBuilder.group({}),
      taxonomies: [[], [Validators.required]],
      menuPriceList: this.formBuilder.array([]),
      order: [0, [Validators.required]]
    });
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.MENU.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.MENU.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
