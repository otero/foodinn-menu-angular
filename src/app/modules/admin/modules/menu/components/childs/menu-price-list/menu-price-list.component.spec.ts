import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuPriceListComponent } from './menu-price-list.component';

describe('MenuPriceListComponent', () => {
  let component: MenuPriceListComponent;
  let fixture: ComponentFixture<MenuPriceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuPriceListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuPriceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
