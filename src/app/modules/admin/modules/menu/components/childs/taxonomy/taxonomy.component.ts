import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { map } from 'rxjs/operators';

import { Location } from 'src/app/modules/core/models/location';
import { Menu } from 'src/app/modules/core/models/menu';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { MenuTaxonomyService } from 'src/app/modules/core/services/menu-taxonomy/menu-taxonomy.service';
import { MenuService } from 'src/app/modules/core/services/menu/menu.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';

@Component({
  selector: 'app-menu-taxonomy',
  templateUrl: './taxonomy.component.html',
  styleUrls: ['./taxonomy.component.css']
})
export class TaxonomyComponent implements OnInit, OnDestroy {

  @Input() parentForm: FormGroup;

  taxonomies: Taxonomy[] = [];
  menu$;
  filtersAny: string = 'status[eq]=active&limit=100000';
  filtersWithOutStatus: string = 'limit=100000'
  defaultLang: string;
  menuCreateOrUpdate$;
  menu: Menu;
  location: Location;
  user: User;

  constructor(
    private locationService: LocationService,
    private taxonomyService: TaxonomyService,
    private authService: AuthService,
    private menuService: MenuService,
    private menuTaxonomyService: MenuTaxonomyService,
    private commonService: CommonService
  ) {
    this.location = this.locationService.loggedLocation;
    this.user = this.authService.loggedUser;
    this.defaultLang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchTaxonomies();
    this.callObservables();
  }

  private callObservables(): void {
    this.menu$ = this.menuService.menu$.subscribe(
      menu => {
        if (menu !== null) {
          this.menu = menu;
          this.fetchTaxonomyByMenu(menu);
        }
      },
      () => {}
    );

    this.menuCreateOrUpdate$ = this.menuService.menuCreateOrUpdate$.subscribe(
      menu => {
        if (menu !== null) {
          this.createOrUpdateMenuTaxonomies(menu, this.parentForm.get('taxonomies').value);
        }
      },
      () => {}
    );
  }

  private fetchTaxonomyByMenu(menu): void {
    this.menuTaxonomyService.getAll(`${this.filtersWithOutStatus}&menu[eq]=${menu.id}`)
      .pipe(
        map(taxonomies => {
          const array = [];
          taxonomies.forEach(taxonomy => {
            array.push(taxonomy.taxonomy.id);
          });
          return array;
        })
      )
      .subscribe(
        taxonomies => {
          this.taxonomiesControl.patchValue(taxonomies);
        },
        () => this.taxonomiesControl.patchValue([])
      );
  }

  private fetchTaxonomies(): void {
    this.taxonomyService.getAll(`${this.filtersAny}&menuRelated[eq]=1&visible[eq]=1`).subscribe(
      taxonomies => {
        this.taxonomies = taxonomies;
      },
      () => {
        this.taxonomies = [];
      }
    );
  }

  async createOrUpdateMenuTaxonomies(menu, taxonomies) {
    await this.deleteAllMenuTaxonomies(menu);
    await taxonomies.forEach(taxonomy => {
      this.menuTaxonomyService.create({
        taxonomy: parseInt(taxonomy),
        menu: parseInt(menu),
        order: 0
      }).subscribe(
        () => {},
        () => {}
      );
    });
  }

  async deleteAllMenuTaxonomies(menu) {
    if (menu === null) {
      return;
    }
    await this.menuTaxonomyService
      .getAll(`${this.filtersWithOutStatus}&menu[eq]=${menu}`).toPromise()
      .then(async menuTaxonomies => {
        const deletePromises = [];
        menuTaxonomies.forEach(menuTaxonomy => {
          let promise = this.menuTaxonomyService.delete(menuTaxonomy.id).toPromise();
          deletePromises.push(promise);
        });
        await Promise.all(deletePromises);
      })
      .catch(() => {});
  }

  get taxonomiesControl() {
    return this.parentForm.controls['taxonomies'];
  }

  ngOnDestroy() {
    this.menuCreateOrUpdate$.unsubscribe();
    this.menu$.unsubscribe();
  }
}
