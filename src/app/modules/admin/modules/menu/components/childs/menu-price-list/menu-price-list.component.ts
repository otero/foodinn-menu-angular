import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Location } from 'src/app/modules/core/models/location';
import { Menu } from 'src/app/modules/core/models/menu';
import { PriceList } from 'src/app/modules/core/models/price-list';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { MenuPriceListService } from 'src/app/modules/core/services/menu-price-list/menu-price-list.service';
import { MenuService } from 'src/app/modules/core/services/menu/menu.service';
import { PriceListService } from 'src/app/modules/core/services/price-list/price-list.service';

@Component({
  selector: 'app-menu-price-list',
  templateUrl: './menu-price-list.component.html',
  styleUrls: ['./menu-price-list.component.css']
})
export class MenuPriceListComponent implements OnInit, OnDestroy {

  @Input() parentForm: FormGroup;
  @Input() isNew: boolean;

  user: User;
  location: Location;
  priceLists: PriceList[] = [];
  filtersAny: string = 'status[eq]=active&limit=100000';
  menu: Menu;
  menu$;
  menuCreateOrUpdate$;
  defaultLang: string;

  constructor(
    private authService: AuthService,
    private locationService: LocationService,
    private formBuilder: FormBuilder,
    private priceListService: PriceListService,
    private menuService: MenuService,
    private commonService: CommonService,
    private menuPriceListService: MenuPriceListService
  ) {
    this.defaultLang = this.commonService.defaultLang;
  }

  async ngOnInit() {
    this.location = this.locationService.loggedLocation;
    this.user = this.authService.loggedUser;
    await this.fetchPriceLists();
    await this.callObservables();

    if (this.isNew) {
      await this.createEmptyMenuPriceList();
    }
  }

  private callObservables(): void {
    this.menu$ = this.menuService.menu$.subscribe(
      menu => {
        if (menu !== null) {
          this.menu = menu;
          this.updateMenuPriceListFormByMenu(menu);
        }
      },
      () => {}
    );

    this.menuCreateOrUpdate$ = this.menuService.menuCreateOrUpdate$.subscribe(
      menu => {
        if (menu !== null) {
          this.createOrUpdateMenuPriceList(menu, this.parentForm.get('menuPriceList').value);
        }
      },
      () => {}
    );
  }

  private createOrUpdateMenuPriceList(menu, menuPriceLists): void {
    menuPriceLists.forEach(menuPriceList => {
      const menuPriceListDTO = this.mapDTOMenuPriceList(menu, menuPriceList);
      if (menuPriceList.id === undefined || menuPriceList.id === null) {
        this.menuPriceListService.create(menuPriceListDTO).subscribe(
          () => {},
          () => {}
        );
        return;
      }
      this.menuPriceListService.update(menuPriceList.id, menuPriceListDTO).subscribe(
        () => {},
        () => {}
      );
    });
  }

  private createEmptyMenuPriceList(): void {
    this.priceLists.forEach(priceList => {
      this.menuPriceListField.push(this.createOrUpdateMenuPriceListForm(
        null,
        0,
        priceList.id,
        priceList.translations[this.user.lang].name
      ));
    });
  }

  async updateMenuPriceListFormByMenu(menu) {
    for (const priceList of this.priceLists) {
      await this.menuPriceListService
        .getAll(`${this.filtersAny}&menu[eq]=${menu.id}&priceList[eq]=${priceList.id}`)
        .pipe(map(menus => {
          return menus[0]
        }))
        .toPromise()
        .then(menuPriceList => {
          this.menuPriceListField.push(this.createOrUpdateMenuPriceListForm(
            menuPriceList.id,
            menuPriceList.price,
            menuPriceList.price_list.id,
            menuPriceList.price_list.translations[this.defaultLang].name
          ));
        })
        .catch(
          () =>{
            this.menuPriceListField.push(this.createOrUpdateMenuPriceListForm(
              null,
              0,
              priceList.id,
              priceList.translations[this.user.lang].name
            ));
          }
        );
    }
  }

  async fetchPriceLists() {
    await this.priceListService.getAll(`${this.filtersAny}&location[eq]=${this.location.id}`).toPromise()
      .then(
        priceLists => {
          this.priceLists = priceLists;
        }
      )
  }

  private createOrUpdateMenuPriceListForm(id, price, priceListId, priceListName): FormGroup {
    const form = this.formBuilder.group({
      id: id,
      price_list: priceListId,
      price: [price, [Validators.required, Validators.pattern("[0-9]+(\.[0-9][0-9]?)?")]],
      price_list_name: priceListName,
    });

    return form;
  }

  get menuPriceListField(): FormArray {
    return this.parentForm.get('menuPriceList') as FormArray;
  }

  private mapDTOMenuPriceList(menuId, menuPriceList): any {
    let menuPriceListDTO = {...menuPriceList};
    menuPriceListDTO = {...menuPriceListDTO, menu: parseFloat(menuId)}
    delete menuPriceListDTO.id;
    delete menuPriceListDTO.price_list_name;

    return menuPriceListDTO;
  }

  ngOnDestroy(): void {
    this.menuCreateOrUpdate$.unsubscribe();
    this.menu$.unsubscribe();
  }
}
