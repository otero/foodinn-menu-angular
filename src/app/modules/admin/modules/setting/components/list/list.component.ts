import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Customer } from 'src/app/modules/core/models/customer';
import { CustomerService } from 'src/app/modules/core/services/customer/customer.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  form: FormGroup;
  customer: Customer;
  _showExternal: boolean = false;

  constructor(
    private customerService: CustomerService,
    private formBuilder: FormBuilder,
    private toastService: ToastService
  ) {
    this.customer = this.customerService.loggedCustomer;
    this.createFormBuilder();

    this.form.patchValue(this.customer);
  }

  doSave(): void {
    this.customerService.update(this.customer.id, this.mapDTO(this.form.value)).subscribe(
      customer => {
        this.customerService.loggedCustomer = customer;
        this.customer = this.customerService.loggedCustomer;
        this.toastService.presentToast('ADMIN.MKT.LIST.SUCCESS');
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR', 'danger');
      }
    );
  }

  showExternal(event) {
    event.preventDefault();
    this._showExternal = !this._showExternal;
  }

  setInternal(): void {
    this.form.get('external_or_internal_feedback').patchValue('internal');
    this.doSave();
  }

  setExternal(event): void {
    this.form.get('external_or_internal_feedback').patchValue('external');
    this.showExternal(event);
  }

  ngOnInit(): void {
  }

  private mapDTO(form) {
    const DTO = { ...form }
    if (DTO.join_us_url == null) {
      delete DTO.join_us_url;
    }
    return DTO;
  }

  private createFormBuilder(): void {
    const urlRegex = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    this.form = this.formBuilder.group({
      name: null,
      description: null,
      passed_tax: null,
      menu: null,
      cookies_policy: null,
      privacy_policy: null,
      legal_note: null,
      external_or_internal_feedback: null,
      external_feedback_url: [null],
      join_us_url: [null, [Validators.required, Validators.pattern(urlRegex)]],
    })
  }
}
