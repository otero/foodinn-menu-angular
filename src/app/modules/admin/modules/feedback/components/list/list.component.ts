import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Feedback } from 'src/app/modules/core/models/feedback';

import { User } from 'src/app/modules/core/models/user';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { FeedbackService } from 'src/app/modules/core/services/feedback/feedback.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  feedbacks: Feedback[] = [];
  displayedColumns: string[] = ['name', 'date', 'rating', 'comment'];
  dataSource: MatTableDataSource<Feedback>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private feedbackService: FeedbackService,
    private toastService: ToastService,
    public dialog: MatDialog,
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchFeedbacks(this.filters);
  }

  private fetchFeedbacks(queryString): void {
    this.feedbackService.getAll(queryString).subscribe(
      feedbacks => {
        this.feedbacks = feedbacks;
        this.dataSource = new MatTableDataSource<Feedback>(this.feedbacks);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.feedbacks = [];
        this.dataSource = new MatTableDataSource<Feedback>(this.feedbacks);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchFeedbacks(this.filters);
  }

  changeLang(lang): void {
    this.lang = lang;
  }
}
