import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Lang } from 'src/app/modules/core/models/lang';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { UtilsService } from 'src/app/modules/core/services/utils/utils.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  langs: Lang[] = [];
  filtersLang: string = 'status[eq]=active&limit=100000';
  lang: string = null;

  @Output() emit = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private langService: LangService,
    private utilService: UtilsService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.fetchLangs();
  }

  doFilter(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const query = this.utilService.objectToQueryStringByFilter(this.mapDTO(this.form.value));
      this.emit.emit({
        isLang: false,
        query: query
      });
      this.form.reset();
    }
  }

  private fetchLangs(): void {
    this.langService.getAll(this.filtersLang).subscribe(
      langs => {
        this.langs = langs;
      },
      () => {
        this.langs = [];
      }
    )
  }

  private mapDTO(form) {
    const DTO = { ...form }
    delete DTO.lang;

    return DTO;
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      rating__eq: null
    });
  }
}
