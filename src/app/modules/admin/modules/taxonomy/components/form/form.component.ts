import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  taxonomyId: string | number;
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  taxonomy: Taxonomy;
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  translationsKeys: string[] = [];
  authUser: User;
  defaultLang: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private locationService: LocationService,
    private authService: AuthService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private taxonomyService: TaxonomyService,
    private commonService: CommonService
  ) {
    this.buildForm();
    this.defaultLang = this.commonService.defaultLang;
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.taxonomyId = params.id;
      this.initPage(this.taxonomyId);
    });
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;
        this.addLangs();
      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          name: ['', [Validators.required]],
          description: ['', [Validators.required]],
        }),
      );
    }
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;

      this.isNew = false;

      await this.fetchLangs();
      await this.fetchTaxonomy(id);
      return;
    }

    this.texts = this.textNew;
    await this.fetchLangs();
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const taxonomyDTO = this.form.value;

      if (this.taxonomyId === undefined) {
        this.createTaxonomy(taxonomyDTO);
        return;
      }
      this.updateTaxonomy(taxonomyDTO);
    }
  }

  private createTaxonomy(DTO): void {
    this.taxonomyService.create(this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.TAXONOMY.CREATE_SUCCESS');
        this.router.navigate(['/admin/taxonomy']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateTaxonomy(DTO): void {
    this.taxonomyService.update(this.taxonomyId, this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.TAXONOMY.UPDATE_SUCCESS');
        this.router.navigate(['/admin/taxonomy']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }

    DTO.order = parseFloat(DTO.order);

    return DTO;
  }

  async fetchTaxonomy(id) {
    await this.taxonomyService.get(id).toPromise()
      .then(taxonomy => {
        this.taxonomy = taxonomy;
        this.form.patchValue(this.taxonomy);
        this.form.get('location').patchValue(this.taxonomy.location.id);
       })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/taxonomy']);
      });
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      visible: [true, [Validators.required]],
      menu_related: [false, [Validators.required]],
      order: [0, [Validators.required]],
      promoted: [false, [Validators.required]],
      translations: this.formBuilder.group({}),
    });
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.TAXONOMY.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.TAXONOMY.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
