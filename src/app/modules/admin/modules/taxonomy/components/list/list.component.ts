import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';

import { User } from 'src/app/modules/core/models/user';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { MenuService } from 'src/app/modules/core/services/menu/menu.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  taxonomies: Taxonomy[] = [];
  displayedColumns: string[] = ['name', 'visible', 'actions'];
  dataSource: MatTableDataSource<Taxonomy>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;


  menu: number = 0;
  product: number = 0;
  taxonomy: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private taxonomyService: TaxonomyService,
    private toastService: ToastService,
    public dialog: MatDialog,
    private menuService: MenuService,
    private productService: ProductService,
    private formBuilder: FormBuilder,
    private locationService: LocationService
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchTaxonomies(this.filters);
    this.fetchTaxonomy();
    this.fetchProduct();
    this.fetchMenu();
  }

  private fetchTaxonomy(): void {
    this.taxonomyService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(taxonomies => {
      this.taxonomy = taxonomies
    });
  }

  private fetchMenu(): void {
    this.menuService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(menus => {
      this.menu = menus;
    });
  }

  private fetchProduct(): void {
    this.productService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(product => {
      this.product = product;
    });
  }

  private fetchTaxonomies(queryString): void {
    this.taxonomyService.getAll(queryString).subscribe(
      taxonomies => {
        this.taxonomies = taxonomies;
        this.dataSource = new MatTableDataSource<Taxonomy>(this.taxonomies);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.taxonomies = [];
        this.dataSource = new MatTableDataSource<Taxonomy>(this.taxonomies);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.taxonomyService.delete(this.deleteId).subscribe(
      () => {
        this.fetchTaxonomies(this.filters);
        this.toastService.presentToast('ADMIN.TAXONOMY.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  changeVisible(id, visible) {
    const taxonomy = this.taxonomies.find(taxonomy => taxonomy.id === id);
    taxonomy.visible = visible ? false : true;
    const form = this.buildForm(taxonomy);
    this.taxonomyService.update(taxonomy.id, this.mapDTO(form.value)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.TAXONOMY.UPDATE_SUCCESS');
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    )
    return taxonomy.visible;
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchTaxonomies(this.filters);
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.TAXONOMY.DELETE_TITLE',
        subtitle: 'ADMIN.TAXONOMY.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }

  private buildForm(taxonomy) {
    const form = this.formBuilder.group({
      translations: this.formBuilder.group({}),
      location: this.locationService.loggedLocation.id,
      visible: taxonomy.visible,
      menu_related: taxonomy.menu_related,
      promoted: [false, [Validators.required]],
      order: taxonomy.order
    });

    Object.keys(taxonomy.translations).forEach(key => {
      this.addTranslation(form, key, taxonomy.translations[key]);
    });

    return form;
  }

  private addTranslation(form, lang, value): void {
    const translation = form.get('translations') as FormGroup;
    translation.addControl(
      lang,
      this.formBuilder.group({
        name: [value.name, [Validators.required]],
        description: [value.description, [Validators.required]]
      }),
    );
  }

  search(event: Event) {
    this.dataSource.filterPredicate = (data, filter: string)  => {
      const accumulator = (currentTerm, key) => {
        return key === 'translations' ? currentTerm + data.translations[this.lang].name : currentTerm + data[key];
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase()
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private mapDTO(form) {
    const DTO = { ...form }

    return DTO;
  }
}
