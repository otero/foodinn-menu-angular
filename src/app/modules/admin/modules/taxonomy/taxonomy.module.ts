import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaxonomyRoutingModule } from './taxonomy-routing.module';
import { ListComponent } from './components/list/list.component';
import { SearchComponent } from './components/search/search.component';
import { FormComponent } from './components/form/form.component';
import { CoreModule } from 'src/app/modules/core/core.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ListComponent, SearchComponent, FormComponent],
  imports: [
    CommonModule,
    TaxonomyRoutingModule,
    CoreModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class TaxonomyModule { }
