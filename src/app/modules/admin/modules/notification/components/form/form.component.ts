import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { User } from 'src/app/modules/core/models/user';
import { Notification } from 'src/app/modules/core/models/notification';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { NotificationService } from 'src/app/modules/core/services/notification/notification.service';
import { Product } from 'src/app/modules/core/models/product';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  notificationId: string | number;
  fileType: string[] = ['image/jpeg', 'image/png'];
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  notification: Notification;
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  translationsKeys: string[] = [];
  authUser: User;
  products: Product[] = [];
  taxonomies: Taxonomy[] = [];
  defaultLang: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private notificationService: NotificationService,
    private locationService: LocationService,
    private authService: AuthService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private productService: ProductService,
    private taxonomyService: TaxonomyService,
    private commonService: CommonService
  ) {
    this.buildForm();
    this.defaultLang = this.commonService.defaultLang;
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.fetchProducts();
    this.fetchTaxonomies();

    this.route.params.subscribe((params: Params) => {
      this.notificationId = params.id;
      this.initPage(this.notificationId);
    });
  }

  private fetchProducts(): void {
    this.productService.getAll(this.filtersAny).subscribe(
      products => {
        this.products = products;
      },
      () => {
        this.products = [];
      }
    );
  }

  private fetchTaxonomies(): void {
    this.taxonomyService.getAll(this.filtersAny).subscribe(
      taxonomies => {
        this.taxonomies = taxonomies;
      },
      () => {
        this.taxonomies = [];
      }
    );
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;
        this.addLangs();
      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          name: ['', [Validators.required]],
          description: ['', [Validators.required]],
        }),
      );
    }
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;

      this.isNew = false;

      await this.fetchLangs();
      await this.fetchNotification(id);
      return;
    }

    this.texts = this.textNew;
    await this.fetchLangs();
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const notificationDTO = this.form.value;

      if (this.notificationId === undefined) {
        this.createNotification(notificationDTO);
        return;
      }
      this.updateNotification(notificationDTO);
    }
  }

  private createNotification(DTO): void {
    this.notificationService.create(this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.NOTIFICATION.CREATE_SUCCESS');
        this.router.navigate(['/admin/notification']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateNotification(DTO): void {
    this.notificationService.update(this.notificationId, this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.NOTIFICATION.UPDATE_SUCCESS');
        this.router.navigate(['/admin/notification']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }

    delete DTO.redirect_type;

    if (DTO.product == null ) { delete DTO.product } else { DTO.product = parseFloat(DTO.product); }
    if (DTO.taxonomy == null ) { delete DTO.taxonomy } else { DTO.taxonomy = parseFloat(DTO.taxonomy); }
    if (DTO.external_link == null ) { delete DTO.external_link }

    return DTO;
  }

  async fetchNotification(id) {
    await this.notificationService.get(id).toPromise()
      .then(notification => {
        this.notification = notification;
        this.form.patchValue(this.notification);
        this.form.get('location').patchValue(this.notification.location.id);
        this.form.get('product').patchValue((this.notification.product != null) ? this.notification.product.id.toString() : null);
        this.form.get('taxonomy').patchValue((this.notification.taxonomy != null) ? this.notification.taxonomy.id.toString() : null);

        if (this.notification.product != null) { this.form.get('redirect_type').patchValue('product')}
        if (this.notification.taxonomy != null) { this.form.get('redirect_type').patchValue('taxonomy')}
        if (this.notification.feedback) { this.form.get('redirect_type').patchValue('appreciation')}
        if (this.notification.external_link != null) { this.form.get('redirect_type').patchValue('external')}
      })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/notification']);
      });
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  sumMinute(): void {
    this.form.get('send_on_minute').patchValue(this.form.get('send_on_minute').value + 1);
  }

  restMinute(): void {
    if (this.form.get('send_on_minute').value == 1) {
      return;
    }
    this.form.get('send_on_minute').patchValue(this.form.get('send_on_minute').value - 1);
  }

  changeRedirectType(event, type): void {
    event.preventDefault();
    $('.btn_').removeClass('active');
    $(event.target).addClass('active');

    this.form.get('redirect_type').patchValue(type);

    if (type == 'appreciation') {
      this.form.get('feedback').patchValue(true);
      this.form.get('product').patchValue(null);
      this.form.get('taxonomy').patchValue(null);
      this.form.get('external_link').patchValue(null);
      return;
    }

    if (type == 'product') {
      this.form.get('taxonomy').patchValue(null);
      this.form.get('external_link').patchValue(null);
    }

    if (type == 'taxonomy') {
      this.form.get('product').patchValue(null);
      this.form.get('external_link').patchValue(null);
    }

    if (type == 'external') {
      this.form.get('product').patchValue(null);
      this.form.get('taxonomy').patchValue(null);
    }

    this.form.get('feedback').patchValue(false);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      visible: [true, [Validators.required]],
      product: [null],
      taxonomy: [null],
      send_on_minute: [10, [Validators.required]],
      external_link: [null],
      feedback: [false, [Validators.required]],
      translations: this.formBuilder.group({}),
      redirect_type: 'product'
    });
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.NOTIFICATION.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.NOTIFICATION.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
