import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { User } from 'src/app/modules/core/models/user';
import { Notification } from 'src/app/modules/core/models/notification';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { NotificationService } from 'src/app/modules/core/services/notification/notification.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  notifications: Notification[] = [];
  displayedColumns: string[] = ['name', 'description', 'minutes', 'visible', 'actions'];
  dataSource: MatTableDataSource<Notification>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private notificationService: NotificationService,
    private locationService: LocationService,
    private toastService: ToastService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchNotifications(this.filters);
  }

  private fetchNotifications(queryString): void {
    this.notificationService.getAll(queryString).subscribe(
      notifications => {
        this.notifications = notifications;
        this.dataSource = new MatTableDataSource<Notification>(this.notifications);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.notifications = [];
        this.dataSource = new MatTableDataSource<Notification>(this.notifications);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.notificationService.delete(this.deleteId).subscribe(
      () => {
        this.fetchNotifications(this.filters);
        this.toastService.presentToast('ADMIN.NOTIFICATION.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchNotifications(this.filters);
  }

  changeVisible(id, visible) {
    const notification = this.notifications.find(notification => notification.id === id);
    notification.visible = visible ? false : true;
    const form = this.buildForm(notification);
    this.notificationService.update(notification.id, this.mapDTO(form.value)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.NOTIFICATION.UPDATE_SUCCESS');
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    )
    return notification.visible;
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.NOTIFICATION.DELETE_TITLE',
        subtitle: 'ADMIN.NOTIFICATION.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }

  private buildForm(notification) {
    const form = this.formBuilder.group({
      translations: this.formBuilder.group({}),
      location: this.locationService.loggedLocation.id,
      visible: notification.visible,
      product: (notification.product != null) ? notification.product.id : null,
      taxonomy: (notification.taxonomy != null) ? notification.taxonomy.id : null,
      send_on_minute: notification.send_on_minute,
      external_link: notification.external_link,
      feedback: notification.feedback
    });

    Object.keys(notification.translations).forEach(key => {
      this.addTranslation(form, key, notification.translations[key]);
    });
    return form;
  }

  private addTranslation(form, lang, value): void {
    const translation = form.get('translations') as FormGroup;
    translation.addControl(
      lang,
      this.formBuilder.group({
        name: [value.name, [Validators.required]],
        description: [value.description, [Validators.required]],
      }),
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }

    delete DTO.redirect_type;

    if (DTO.product == null ) { delete DTO.product } else { DTO.product = parseFloat(DTO.product); }
    if (DTO.taxonomy == null ) { delete DTO.taxonomy } else { DTO.taxonomy = parseFloat(DTO.taxonomy); }
    if (DTO.external_link == null ) { delete DTO.external_link }

    return DTO;
  }

  search(event: Event) {
    this.dataSource.filterPredicate = (data, filter: string)  => {
      const accumulator = (currentTerm, key) => {
        return key === 'translations' ? currentTerm + data.translations[this.lang].name : currentTerm + data[key];
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase()
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
