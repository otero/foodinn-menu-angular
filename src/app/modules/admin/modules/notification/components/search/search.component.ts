import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Lang } from 'src/app/modules/core/models/lang';
import { Product } from 'src/app/modules/core/models/product';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';
import { UtilsService } from 'src/app/modules/core/services/utils/utils.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  langs: Lang[] = [];
  filtersAny: string = 'status[eq]=active&limit=100000';
  lang: string = null;
  products: Product[] = [];
  taxonomies: Taxonomy[] = [];
  defaultLang: string;

  @Output() emit = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private langService: LangService,
    private taxonomyService: TaxonomyService,
    private productService: ProductService,
    private commonService: CommonService,
    private utilService: UtilsService
  ) {
    this.buildForm();
    this.defaultLang = this.commonService.defaultLang;
    this.form.get('lang').valueChanges.subscribe(result => {
      if (result != this.lang && result != null) {
        this.emit.emit({
          isLang: true,
          lang: result
        });
        this.lang = result;
      }
    });
  }

  ngOnInit(): void {
    this.fetchLangs();
    this.fetchTaxonomies();
    this.fetchProducts();
  }

  doFilter(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const query = this.utilService.objectToQueryStringByFilter(this.mapDTO(this.form.value));
      this.emit.emit({
        isLang: false,
        query: query
      });
      this.form.reset();
    }
  }

  private fetchProducts(): void {
    this.productService.getAll(this.filtersAny).subscribe(
      products => {
        this.products = products;
      },
      () => {
        this.products = [];
      }
    );
  }

  private fetchTaxonomies(): void {
    this.taxonomyService.getAll(this.filtersAny).subscribe(
      taxonomies => {
        this.taxonomies = taxonomies;
      },
      () => {
        this.taxonomies = [];
      }
    );
  }

  private fetchLangs(): void {
    this.langService.getAll(this.filtersAny).subscribe(
      langs => {
        this.langs = langs;
      },
      () => {
        this.langs = [];
      }
    )
  }

  private mapDTO(form) {
    const DTO = { ...form }
    delete DTO.lang;

    return DTO;
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      status__eq: 'active',
      visible__eq: 1,
      feedback__eq: 0,
      product__eq: null,
      taxonomy__eq: null,
      lang: []
    });
  }
}
