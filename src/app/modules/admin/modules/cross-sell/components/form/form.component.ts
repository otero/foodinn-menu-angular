import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { Product } from 'src/app/modules/core/models/product';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { CrossSellService } from 'src/app/modules/core/services/cross-sell/cross-sell.service';
import { CrossSell } from 'src/app/modules/core/models/cross-sell';
import { CommonService } from 'src/app/modules/core/services/common/common.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  crossSellId: string | number;
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  crossSell: CrossSell;
  products: Product[] = [];
  filtersAny: string = 'status[eq]=active&limit=100000';
  authUser: User;
  defaultLang: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private crossSellService: CrossSellService,
    private productService: ProductService,
    private commonService: CommonService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.defaultLang = this.commonService.defaultLang;
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.crossSellId = params.id;
      this.buildForm();
      this.initPage(this.crossSellId);
      this.fetchProducts();
    });
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;

      this.isNew = false;
      await this.fetchCrossSell(id);
      return;
    }

    this.texts = this.textNew;
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const crossSellDTO = this.form.value;

      if (this.crossSellId === undefined) {
        crossSellDTO.cross_sell_product.forEach(product => {
          this.createCrossSell({
            visible: crossSellDTO.visible,
            product: parseInt(crossSellDTO.product),
            cross_sell_product: parseInt(product)
          });
        });


        return;
      }
      this.updateCrossSell({
        visible: crossSellDTO.visible,
        product: parseInt(crossSellDTO.product),
        cross_sell_product: parseInt(crossSellDTO.cross_sell_product)
      });
    }
  }

  private fetchProducts(): void {
    this.productService.getAll(this.filtersAny).subscribe(
      products => {
        this.products = products
      },
      () => {
        this.products = [];
      }
    );
  }

  private createCrossSell(DTO): void {
    this.crossSellService.create(DTO).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.CROSS_SELL.CREATE_SUCCESS');
        this.router.navigate(['/admin/cross-sell']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateCrossSell(DTO): void {
    this.crossSellService.update(this.crossSellId, DTO).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.CROSS_SELL.UPDATE_SUCCESS');
        this.router.navigate(['/admin/cross-sell']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  async fetchCrossSell(id) {
    await this.crossSellService.get(id).toPromise()
      .then(crossSell => {
        this.crossSell = crossSell;
        this.form.patchValue(this.crossSell);
        this.form.get('product').patchValue(this.crossSell.product.id);
        this.form.get('cross_sell_product').patchValue(this.crossSell.cross_sell_product.id);
      })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/cross-sell']);
      });
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      product: [null, [Validators.required]],
      cross_sell_product: [(this.crossSellId === undefined) ? [] : null, [Validators.required]],
      visible: [true, [Validators.required]]
    });
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.CROSS_SELL.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.CROSS_SELL.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
