import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Lang } from 'src/app/modules/core/models/lang';
import { Product } from 'src/app/modules/core/models/product';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { UtilsService } from 'src/app/modules/core/services/utils/utils.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  form: FormGroup;
  langs: Lang[] = [];
  products: Product[] = [];
  filtersLang: string = 'status[eq]=active&limit=100000';
  lang: string = null;
  defaultLang: string;

  @Output() emit = new EventEmitter<any>();

  constructor(
    private formBuilder: FormBuilder,
    private langService: LangService,
    private commonService: CommonService,
    private utilService: UtilsService,
    private productService: ProductService
  ) {
    this.buildForm();
    this.defaultLang = this.commonService.defaultLang;

    this.form.get('lang').valueChanges.subscribe(result => {
      if (result != this.lang && result != null) {
        this.emit.emit({
          isLang: true,
          lang: result
        });
        this.lang = result;
      }
    });
  }

  ngOnInit(): void {
    this.fetchLangs();
    this.fetchProducts();
  }

  private fetchProducts(): void {
    this.productService.getAll(this.filtersLang).subscribe(
      products => {
        this.products = products;
      },
      () => {
        this.products = [];
      }
    );
  }

  doFilter(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const query = this.utilService.objectToQueryStringByFilter(this.mapDTO(this.form.value));
      this.emit.emit({
        isLang: false,
        query: query
      });
      this.form.reset();
    }
  }

  private fetchLangs(): void {
    this.langService.getAll(this.filtersLang).subscribe(
      langs => {
        this.langs = langs;
      },
      () => {
        this.langs = [];
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }
    delete DTO.lang;

    return DTO;
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      status__eq: 'active',
      visible__eq: [],
      product__eq: [],
      cross_sell_product__eq: [],
      lang: []
    });
  }
}
