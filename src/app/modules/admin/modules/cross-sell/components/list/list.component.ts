import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { CrossSell } from 'src/app/modules/core/models/cross-sell';

import { User } from 'src/app/modules/core/models/user';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { CrossSellService } from 'src/app/modules/core/services/cross-sell/cross-sell.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  crossSells: CrossSell[] = [];
  displayedColumns: string[] = ['product', 'cross_sell_product', 'visible', 'actions'];
  dataSource: MatTableDataSource<CrossSell>;
  deleteId: number;
  filters: string = 'limit=100000';
  authUser: User;
  lang: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private crossSellService: CrossSellService,
    private toastService: ToastService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetcCrossSells(this.filters);
  }

  private fetcCrossSells(queryString): void {
    this.crossSellService.getAll(queryString).subscribe(
      crossSell => {
        this.crossSells = crossSell;
        this.dataSource = new MatTableDataSource<CrossSell>(this.crossSells);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.crossSells = [];
        this.dataSource = new MatTableDataSource<CrossSell>(this.crossSells);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.crossSellService.delete(this.deleteId).subscribe(
      () => {
        this.fetcCrossSells(this.filters);
        this.toastService.presentToast('ADMIN.CROSS_SELL.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetcCrossSells(this.filters);
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  changeVisible(id, visible) {
    const crossSell = this.crossSells.find(crossSell => crossSell.id === id);
    crossSell.visible = visible ? false : true;
    const form = this.buildForm(crossSell);
    this.crossSellService.update(crossSell.id, this.mapDTO(form.value)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.CROSS_SELL.UPDATE_SUCCESS');
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    )
    return crossSell.visible;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.CROSS_SELL.DELETE_TITLE',
        subtitle: 'ADMIN.CROSS_SELL.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }
  private buildForm(crossSell) {
    const form = this.formBuilder.group({
      product: crossSell.product,
      product_cross_sell: crossSell.product_cross_sell,
      visible: crossSell.visible
    });

    return form;
  }

  private mapDTO(form) {
    const DTO = { ...form }

    return DTO;
  }
}
