import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrossSellRoutingModule } from './cross-sell-routing.module';
import { ListComponent } from './components/list/list.component';
import { FormComponent } from './components/form/form.component';
import { SearchComponent } from './components/search/search.component';
import { CoreModule } from 'src/app/modules/core/core.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [ListComponent, FormComponent, SearchComponent],
  imports: [
    CommonModule,
    CrossSellRoutingModule,
    CoreModule,
    SharedModule,
    ReactiveFormsModule,
    MaterialModule,
    NgSelectModule
  ]
})
export class CrossSellModule { }
