import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { PriceList } from 'src/app/modules/core/models/price-list';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { PriceListService } from 'src/app/modules/core/services/price-list/price-list.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  priceListId: string | number;
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  priceList: PriceList;
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  translationsKeys: string[] = [];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private priceListService: PriceListService,
    private locationService: LocationService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.fetchLangs();
    this.route.params.subscribe((params: Params) => {
      this.priceListId = params.id;
      this.initPage(this.priceListId);
    });
  }

  private fetchLangs(): void {
    this.langService.getAll(this.filtersAny).subscribe(
      langs => {
        this.langs = langs;
        this.addLangs();
      },
      () => {
        this.langs = [];
      }
    );
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          name: ['', [Validators.required]],
          description: ['', [Validators.required]],
        }),
      );
    }
  }

  private initPage(id: string | number): void {
    if (id) {
      this.texts = this.textEdit;
      this.fetchPriceList(id);
      this.isNew = false;
      return;
    }

    this.texts = this.textNew;
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const priceListDTO = this.form.value;

      if (this.priceListId === undefined) {
        this.createPriceList(priceListDTO);
        return;
      }
      this.updatePriceList(priceListDTO);
    }
  }

  private createPriceList(DTO): void {
    this.priceListService.create(DTO).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.PRICE_LIST.CREATE_SUCCESS');
        this.router.navigate(['/admin/price-list']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updatePriceList(DTO): void {
    this.priceListService.update(this.priceListId, DTO).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.PRICE_LIST.UPDATE_SUCCESS');
        this.router.navigate(['/admin/price-list']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private fetchPriceList(id): void {
    this.priceListService.get(id).subscribe(
      priceList => {
        this.priceList = priceList;
        this.form.patchValue(this.priceList);
      },
      () => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/price-list']);
      }
    );
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      status: ['active'],
      translations: this.formBuilder.group({})
    });
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.PRICE_LIST.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.PRICE_LIST.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
