import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';

import { PriceList } from 'src/app/modules/core/models/price-list';
import { User } from 'src/app/modules/core/models/user';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { PriceListService } from 'src/app/modules/core/services/price-list/price-list.service';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  priceLists: PriceList[] = [];
  displayedColumns: string[] = ['name', 'description', 'status', 'actions'];
  dataSource: MatTableDataSource<PriceList>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;

  priceList: number = 0;
  saleCenters: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private priceListService: PriceListService,
    private commonService: CommonService,
    private toastService: ToastService,
    private salesCenterService: SalesCenterService,
    public dialog: MatDialog
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchLPriceLists(this.filters);
    this.fetchSalesCenter();
    this.fetchPriceLists();
  }

  private fetchPriceLists(): void {
    this.priceListService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(priceLists => {
      this.priceList = priceLists
    });
  }

  private fetchSalesCenter(): void {
    this.salesCenterService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(salesCenters => {
      this.saleCenters = salesCenters
    });
  }

  private fetchLPriceLists(queryString): void {
    this.priceListService.getAll(queryString).subscribe(
      langs => {
        this.priceLists = langs;
        this.dataSource = new MatTableDataSource<PriceList>(this.priceLists);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.priceLists = [];
        this.dataSource = new MatTableDataSource<PriceList>(this.priceLists);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.priceListService.delete(this.deleteId).subscribe(
      () => {
        this.fetchLPriceLists(this.filters);
        this.toastService.presentToast('ADMIN.PRICE_LIST.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchLPriceLists(this.filters);
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.PRICE_LIST.DELETE_TITLE',
        subtitle: 'ADMIN.PRICE_LIST.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }
}
