import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { AdService } from 'src/app/modules/core/services/ad/ad.service';
import { Ad } from 'src/app/modules/core/models/ad';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';
import { Product } from 'src/app/modules/core/models/product';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { CommonService } from 'src/app/modules/core/services/common/common.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  adId: string | number;
  adType: string;
  fileType: string[] = ['image/jpeg', 'image/png'];
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  ad: Ad;
  products: Product[] = [];
  taxonomies: Taxonomy[] = [];
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  translationsKeys: string[] = [];
  authUser: User;
  defaultLang: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private adService: AdService,
    private locationService: LocationService,
    private authService: AuthService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private productService: ProductService,
    private taxonomyService: TaxonomyService,
    private commonService: CommonService
  ) {
    this.defaultLang = this.commonService.defaultLang;
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.fetchProducts();
    this.fetchTaxonomies();

    this.route.params.subscribe((params: Params) => {
      this.adId = params.id;
      this.adType = params.type;
      this.buildForm();

      if (this.adType != 'image' && this.adType != 'product' && this.adType != 'taxonomy') {
        this.adType = 'image';
        this.toastService.presentToast('COMMON.ERROR');
        this.router.navigate(['/admin/ad']);
      }
      this.initPage(this.adId);
    });
  }

  private fetchProducts(): void {
    this.productService.getAll(this.filtersAny).subscribe(
      products => {
        this.products = products;
      },
      () => {
        this.products = [];
      }
    );
  }

  private fetchTaxonomies(): void {
    this.taxonomyService.getAll(this.filtersAny).subscribe(
      taxonomies => {
        this.taxonomies = taxonomies;
      },
      () => {
        this.taxonomies = [];
      }
    );
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;
        this.addLangs();
      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          name: ['', [Validators.required]],
          description: ['', [Validators.required]],
          cta: ['', [Validators.required]],
        }),
      );
    }
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;

      this.isNew = false;

      await this.fetchLangs();
      await this.fetchAd(id);
      return;
    }

    this.texts = this.textNew;
    await this.fetchLangs();
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const adDTO = this.form.value;

      if (this.adId === undefined) {
        this.createAd(adDTO);
        return;
      }
      this.updateAd(adDTO);
    }
  }

  private createAd(DTO): void {
    this.adService.create(this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.AD.CREATE_SUCCESS');
        this.router.navigate(['/admin/ad']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateAd(DTO): void {
    this.adService.update(this.adId, this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.AD.UPDATE_SUCCESS');
        this.router.navigate(['/admin/ad']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }

    delete DTO.redirect_type;

    if (DTO.products_list == null ) { delete DTO.products_list }
    if (DTO.taxonomies_list == null ) { delete DTO.taxonomies_list }
    if (DTO.external_link == null ) { delete DTO.external_link }

    if (DTO.image_url == null) {
      delete DTO.image_url;
    }

    return DTO;
  }

  async fetchAd(id) {
    await this.adService.get(id).toPromise()
      .then(ad => {
        this.ad = ad;
        this.form.patchValue(this.ad);
        this.form.get('location').patchValue(this.ad.location.id);

        this.form.get('location').patchValue(this.ad.location.id);
        this.form.get('products_list').patchValue((this.ad.products_list != null) ? this.ad.products_list : null);
        this.form.get('taxonomies_list').patchValue((this.ad.taxonomies_list != null) ? this.ad.taxonomies_list : null);

        if (this.ad.products_list != null) { this.form.get('redirect_type').patchValue('product')}
        if (this.ad.taxonomies_list != null) { this.form.get('redirect_type').patchValue('taxonomy')}
        if (this.ad.external_link != null) { this.form.get('redirect_type').patchValue('external')}
      })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/ad']);
      });
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  changeRedirectType(event, type): void {
    event.preventDefault();
    $('.btn_').removeClass('active');
    $(event.target).addClass('active');

    this.form.get('redirect_type').patchValue(type);

    if (type == 'product') {
      this.form.get('taxonomies_list').patchValue(null);
      this.form.get('external_link').patchValue(null);
    }

    if (type == 'taxonomy') {
      this.form.get('products_list').patchValue(null);
      this.form.get('external_link').patchValue(null);
    }

    if (type == 'external') {
      this.form.get('products_list').patchValue(null);
      this.form.get('taxonomy').patchValue(null);
    }
  }

  setFile(file): void {
    this.form.get('image_url').patchValue(file.file);
  }

  sumMinute(): void {
    this.form.get('show_on_minute').patchValue(this.form.get('show_on_minute').value + 1);
  }

  restMinute(): void {
    if (this.form.get('show_on_minute').value == 1) {
      return;
    }
    this.form.get('show_on_minute').patchValue(this.form.get('show_on_minute').value - 1);
  }

  private buildForm(): void {
    let type = '';
    let redirect_type = 'product'
    if (this.adType == 'product') { type = 'products_list' }
    if (this.adType == 'taxonomy') { type = 'products_list'; redirect_type = 'taxonomy' }
    if (this.adType == 'image') { type = 'image';  redirect_type = 'image'}

    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      products_list: [],
      taxonomies_list: [],
      image_url: [],
      show_on_minute: [10, [Validators.required]],
      external_link: [],
      type: type,
      visible: [true, [Validators.required]],
      translations: this.formBuilder.group({}),
      redirect_type: redirect_type
    });

    if (this.adType == 'image') {
      this.form.get('image_url').setValidators([Validators.required]);
    }
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.AD.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.AD.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
