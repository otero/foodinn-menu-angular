import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

import { User } from 'src/app/modules/core/models/user';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';
import { Ad } from 'src/app/modules/core/models/ad';
import { AdService } from 'src/app/modules/core/services/ad/ad.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  ads: Ad[] = [];
  displayedColumns: string[] = ['name', 'description', 'type', 'visible', 'actions'];
  dataSource: MatTableDataSource<Ad>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private adService: AdService,
    private locationService: LocationService,
    private toastService: ToastService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchAd(this.filters);
  }

  private fetchAd(queryString): void {
    this.adService.getAll(queryString).subscribe(
      ads => {
        this.ads = ads;
        this.dataSource = new MatTableDataSource<Ad>(this.ads);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.ads = [];
        this.dataSource = new MatTableDataSource<Ad>(this.ads);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.adService.delete(this.deleteId).subscribe(
      () => {
        this.fetchAd(this.filters);
        this.toastService.presentToast('ADMIN.AD.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchAd(this.filters);
  }

  changeVisible(id, visible) {
    const ad = this.ads.find(ad => ad.id === id);
    ad.visible = visible ? false : true;
    const form = this.buildForm(ad);
    this.adService.update(ad.id, this.mapDTO(form.value)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.AD.UPDATE_SUCCESS');
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    )
    return ad.visible;
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.AD.DELETE_TITLE',
        subtitle: 'ADMIN.AD.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }

  private buildForm(ad) {
    const form = this.formBuilder.group({
      translations: this.formBuilder.group({}),
      location: this.locationService.loggedLocation.id,
      visible: ad.visible,
      products_list: [(ad.products_list != null) ? ad.products_list: null],
      taxonomies_list: [(ad.taxonomies_list != null) ? ad.taxonomies_list: null],
      show_on_minute: ad.show_on_minute,
      external_link: ad.external_link,
      image_url: ad.image_url,
      type: ad.type
    });

    Object.keys(ad.translations).forEach(key => {
      this.addTranslation(form, key, ad.translations[key]);
    });

    return form;
  }

  private addTranslation(form, lang, value): void {
    const translation = form.get('translations') as FormGroup;
    translation.addControl(
      lang,
      this.formBuilder.group({
        name: [value.name, [Validators.required]],
        description: [value.description, [Validators.required]],
        cta: [value.cta, [Validators.required]],
      }),
    );
  }

  search(event: Event) {
    this.dataSource.filterPredicate = (data, filter: string)  => {
      const accumulator = (currentTerm, key) => {
        return key === 'translations' ? currentTerm + data.translations[this.lang].name : currentTerm + data[key];
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase()
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  private mapDTO(form) {
    const DTO = { ...form }

    delete DTO.redirect_type;

    if (DTO.products_list == null ) { delete DTO.products_list }
    if (DTO.taxonomies_list == null ) { delete DTO.taxonomies_list }
    if (DTO.external_link == null ) { delete DTO.external_link }
    if (DTO.image_url == null ) { delete DTO.image_url }

    return DTO;
  }
}
