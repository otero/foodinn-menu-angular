import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { ListComponent } from './components/list/list.component';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { CoreModule } from 'src/app/modules/core/core.module';


@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    CoreModule
  ]
})
export class HomeModule { }
