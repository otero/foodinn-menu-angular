import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QRCodeModule } from 'angularx-qrcode';

import { SalesCenterRoutingModule } from './sales-center-routing.module';
import { ListComponent } from './components/list/list.component';
import { FormComponent } from './components/form/form.component';
import { SearchComponent } from './components/search/search.component';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { CoreModule } from 'src/app/modules/core/core.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { QrComponent } from './components/qr/qr.component';


@NgModule({
  declarations: [ListComponent, FormComponent, SearchComponent, QrComponent],
  imports: [
    CommonModule,
    SalesCenterRoutingModule,
    MaterialModule,
    CoreModule,
    SharedModule,
    QRCodeModule,
    ReactiveFormsModule
  ]
})
export class SalesCenterModule { }
