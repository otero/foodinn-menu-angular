import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';

import { SalesCenter } from 'src/app/modules/core/models/sales-center';
import { User } from 'src/app/modules/core/models/user';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { PriceListService } from 'src/app/modules/core/services/price-list/price-list.service';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';
import { DialogComponent } from 'src/app/modules/shared/components/dialog/dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  salesCenters: SalesCenter[] = [];
  displayedColumns: string[] = ['name', 'actions'];
  dataSource: MatTableDataSource<SalesCenter>;
  deleteId: number;
  filters: string = 'status[eq]=active&limit=100000';
  authUser: User;
  lang: string;

  priceLists: number = 0;
  saleCenters: number = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private commonService: CommonService,
    private salesCenterService: SalesCenterService,
    private toastService: ToastService,
    private priceListService: PriceListService,
    public dialog: MatDialog
  ) {
    this.lang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.fetchLSalesCenter(this.filters);
    this.fetchSalesCenter();
    this.fetchPriceLists();
  }

  private fetchPriceLists(): void {
    this.priceListService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(priceLists => {
      this.priceLists = priceLists
    });
  }

  private fetchSalesCenter(): void {
    this.salesCenterService.getAll(this.filters).pipe(
      map(response => {
        return response.length;
      })
    ).subscribe(salesCenters => {
      this.saleCenters = salesCenters
    });
  }

  private fetchLSalesCenter(queryString): void {
    this.salesCenterService.getAll(queryString).subscribe(
      salesCenters => {
        this.salesCenters = salesCenters;
        this.dataSource = new MatTableDataSource<SalesCenter>(this.salesCenters);
        this.dataSource.paginator = this.paginator;
      },
      () => {
        this.salesCenters = [];
        this.dataSource = new MatTableDataSource<SalesCenter>(this.salesCenters);
        this.dataSource.paginator = this.paginator;
        this.toastService.presentToast('COMMON.NOT_RESULTS');
      }
    );
  }

  private delete(): void {
    this.salesCenterService.delete(this.deleteId).subscribe(
      () => {
        this.fetchLSalesCenter(this.filters);
        this.toastService.presentToast('ADMIN.SALES_CENTER.DELETE_SUCCESS');
      },
      () =>  {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  setFilter(event): void {
    if (event.isLang) {
      this.changeLang(event.lang);
      return;
    }

    this.filters = event.query;
    this.fetchLSalesCenter(this.filters);
  }

  changeLang(lang): void {
    this.lang = lang;
  }

  openDelete(id): void {
    this.deleteId = id;
    const dialogRef = this.dialog.open(DialogComponent, {
      data: {
        title: 'ADMIN.SALES_CENTER.DELETE_TITLE',
        subtitle: 'ADMIN.SALES_CENTER.DELETE_SUBTITLE'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
     if (result) {
       this.delete();
     }
    });
  }
}
