import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Lang } from 'src/app/modules/core/models/lang';
import { PriceList } from 'src/app/modules/core/models/price-list';
import { SalesCenter } from 'src/app/modules/core/models/sales-center';
import { User } from 'src/app/modules/core/models/user';
import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { LangService } from 'src/app/modules/core/services/lang/lang.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { PriceListService } from 'src/app/modules/core/services/price-list/price-list.service';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  salesCenterId: string | number;
  texts: any;
  isNew: boolean = true;
  form: FormGroup;
  salesCenter: SalesCenter;
  filtersAny: string = 'status[eq]=active&limit=100000';
  langs: Lang[] = [];
  priceLists: PriceList[] = [];
  translationsKeys: string[] = [];
  authUser: User;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private translateService: TranslateService,
    private salesCenterService: SalesCenterService,
    private locationService: LocationService,
    private priceListService: PriceListService,
    private authService: AuthService,
    private langService: LangService,
    private route: ActivatedRoute,
    private toastService: ToastService
  ) {
    this.buildForm();
    this.authUser = this.authService.loggedUser;
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.salesCenterId = params.id;
      this.initPage(this.salesCenterId);
    });
  }

  async fetchPriceLists() {
    await this.priceListService.getAll(this.filtersAny).toPromise()
      .then(priceLists => {
        this.priceLists = priceLists;
      })
      .catch(() => {
        this.priceLists = [];
      });
  }

  async fetchLangs() {
    await this.langService.getAll(this.filtersAny).toPromise()
      .then(langs => {
        this.langs = langs;
        this.addLangs();
      })
      .catch(() => {
        this.langs = [];
      });
  }

  private addLangs(): void {
    for (let lang of this.langs) {
      this.translationsKeys.push(lang.iso);
      this.translations.addControl(
        lang.iso,
        this.formBuilder.group({
          name: ['', [Validators.required]],
          description: ['', [Validators.required]],
        }),
      );
    }
  }

  async initPage(id) {
    if (id) {
      this.texts = this.textEdit;
      this.isNew = false;
      await this.fetchLangs();
      await this.fetchPriceLists();
      await this.fetchSalesCenter(id);
      return;
    }

    this.texts = this.textNew;
    await this.fetchLangs();
    await this.fetchPriceLists();
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      const salesCenterDTO = this.form.value;

      if (this.salesCenterId === undefined) {
        this.createSalesCenter(salesCenterDTO);
        return;
      }
      this.updateSalesCenter(salesCenterDTO);
    }
  }

  private createSalesCenter(DTO): void {
    this.salesCenterService.create(this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.SALES_CENTER.CREATE_SUCCESS');
        this.router.navigate(['/admin/sales-center']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private updateSalesCenter(DTO): void {
    this.salesCenterService.update(this.salesCenterId, this.mapDTO(DTO)).subscribe(
      () => {
        this.toastService.presentToast('ADMIN.SALES_CENTER.UPDATE_SUCCESS');
        this.router.navigate(['/admin/sales-center']);
      },
      () => {
        this.toastService.presentToast('COMMON.ERROR');
      }
    );
  }

  private mapDTO(form) {
    const DTO = { ...form }

    DTO.price_list = parseFloat(DTO.price_list);

    return DTO;
  }

  async fetchSalesCenter(id) {
    await this.salesCenterService.get(id).toPromise()
      .then(salesCenter => {
        this.salesCenter = salesCenter;
        this.form.patchValue(this.salesCenter);
        this.form.get('price_list').patchValue(this.salesCenter.price_list.id.toString());
        this.form.get('location').patchValue(this.salesCenter.location.id);
      })
      .catch(() => {
        this.toastService.presentToast('COMMON.NOT_AVAILABLE');
        this.router.navigate(['/admin/sales-center']);
      });
  }

  getNameTranslation(key): string {
    const lang =  this.langs.find(lang => lang.iso === key);

    if(lang != undefined) {
      return lang.name;
    }

    return null;
  }

  getTransControl(key) {
    return this.form.controls['translations'].get(key);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      price_list: [null, [Validators.required]],
      translations: this.formBuilder.group({})
    });
  }

  get translations(): FormGroup {
    return this.form.get('translations') as FormGroup;
  }

  get textNew(): object {
    return {
      title: this.translateService.instant('ADMIN.SALES_CENTER.TITLE_NEW'),
      button: this.translateService.instant('COMMON.CREATE'),
    }
  }

  get textEdit(): object {
    return {
      title: this.translateService.instant('ADMIN.SALES_CENTER.TITLE_EDIT'),
      button: this.translateService.instant('COMMON.EDIT'),
    }
  }
}
