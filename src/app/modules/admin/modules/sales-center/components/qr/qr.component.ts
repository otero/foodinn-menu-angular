import { Component, OnInit, ViewChild } from '@angular/core';
import { QRCodeComponent } from 'angularx-qrcode';
import { Location } from 'src/app/modules/core/models/location';
import { SalesCenter } from 'src/app/modules/core/models/sales-center';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { UtilsService } from 'src/app/modules/core/services/utils/utils.service';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.css']
})
export class QrComponent implements OnInit {

  filters: string = 'status[eq]=active&limit=100000';
  salesCenters: SalesCenter[] = [];
  path: string;
  location: Location;

  constructor(
    private salesCenterService: SalesCenterService,
    private locationService: LocationService,
    private utilsService: UtilsService
  ) { }

  ngOnInit(): void {
    this.path = this.utilsService.generatePathQR();
    this.location = this.locationService.loggedLocation;
    this.fetchSalesCenters();
  }

  downloadQR(parent, saleCenter) {
    this.utilsService.saveAsImage(parent, saleCenter);
  }

  private fetchSalesCenters(): void {
    this.salesCenterService.getAll(this.filters).subscribe(
      salesCenters => {
        this.salesCenters = salesCenters;
      },
      () => {
        this.salesCenters = [];
      }
    );
  }
}
