import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './components/form/form.component';
import { ListComponent } from './components/list/list.component';
import { QrComponent } from './components/qr/qr.component';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'new', component: FormComponent },
  { path: 'edit/:id', component: FormComponent},
  { path: 'qr', component: QrComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesCenterRoutingModule { }
