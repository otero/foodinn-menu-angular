import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Product } from 'src/app/modules/core/models/product';
import { ProductTag } from 'src/app/modules/core/models/product-tag';
import { Tag } from 'src/app/modules/core/models/tag';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { ProductTagService } from 'src/app/modules/core/services/product-tag/product-tag.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { TagService } from 'src/app/modules/core/services/tag/tag.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';

@Component({
  selector: 'app-product-tag',
  templateUrl: './product-tag.component.html',
  styleUrls: ['./product-tag.component.css']
})
export class ProductTagComponent implements OnInit {

  products: Product[] = [];
  tag: Tag;
  defaultLang: string;
  productTags = [];
  form: FormGroup;
  filtersAny: string = 'limit=100000';
  tagId: string | number;

  constructor(
    private productService: ProductService,
    private tagService: TagService,
    private productTagService: ProductTagService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private toastService: ToastService,
    private commonService: CommonService
  ) {
    this.buildForm();
    this.defaultLang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.tagId = params.id;
      this.initPage();
    });
  }

  async initPage() {
    await this.fetchTag();
    await this.fetchProducts();
    await this.fetchProductTag();
  }

  async fetchTag() {
    await this.tagService.get(this.tagId).toPromise()
      .then(
        tag => {
          this.tag = tag;
          this.form.get('tag').patchValue(this.tag.id);
        }
      )
      .catch(() => {
        this.toastService.presentToast('COMMON.ERROR');
        this.router.navigate(['/admin/tag']);
      });
  }

  async fetchProducts() {
    await this.productService.getAll(this.filtersAny).toPromise()
      .then(
        products => {
          this.products = products;
        }
      )
      .catch(() => {
        this.products = [];
      })
  }

  async fetchProductTag() {
    await this.productTagService.getAll(`${this.filtersAny}&tag[eq]=${this.tagId}`)
    .pipe(
      map(productsTags => {
        const array = [];
        this.productTags = [];
        productsTags.forEach(productTag => {
          this.productTags.push(productTag.id);
          array.push(productTag.product.id);
        });
        return array;
      })
    )
    .toPromise()
      .then(
        productTags => {
         this.form.get('products').patchValue(productTags);
        },
      )
      .catch(() => {
        this.form.get('products').patchValue([]);
        this.productTags = [];
      });
  }

  doSave(event): void {
    event.preventDefault();

    if (this.form.valid) {
      this.createTags();
      this.toastService.presentToast('ADMIN.PRODUCT_TAG.UPDATE_SUCCESS');
      this.router.navigate(['/admin/tag']);
    }
  }

  async createTags() {
    await this.deleteAllProductTag();

    await this.form.get('products').value.forEach(product => {
      this.productTagService.create({
        product: product,
        tag: this.form.get('tag').value,
        visible: this.form.get('visible').value
      }).subscribe(
        () => {},
        () => {}
      );
    });
  }

  async deleteAllProductTag() {
    const deletePromises = [];
    for (let productTag of this.productTags) {
      let promise = this.productTagService.delete(productTag).toPromise();
      deletePromises.push(promise);
    }
    await Promise.all(deletePromises);
  }

  private buildForm(): void {
    this.form = this.formBuilder.group({
      products: [[], Validators.required],
      tag: [null, Validators.required],
      visible: true
    });
  }
}
