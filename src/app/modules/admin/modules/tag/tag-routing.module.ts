import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './components/form/form.component';
import { ListComponent } from './components/list/list.component';
import { ProductTagComponent } from './components/product-tag/product-tag.component';

const routes: Routes = [
  { path: '', component: ListComponent},
  { path: 'new',  component: FormComponent},
  { path: 'edit/:id', component: FormComponent},
  { path: 'product-tag/:id', component: ProductTagComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TagRoutingModule { }
