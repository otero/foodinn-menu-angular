import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TagRoutingModule } from './tag-routing.module';
import { ListComponent } from './components/list/list.component';
import { SearchComponent } from './components/search/search.component';
import { FormComponent } from './components/form/form.component';
import { CoreModule } from 'src/app/modules/core/core.module';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ColorSketchModule } from 'ngx-color/sketch';
import { ProductTagComponent } from './components/product-tag/product-tag.component';
import { NgSelectModule } from '@ng-select/ng-select';



@NgModule({
  declarations: [ListComponent, SearchComponent, FormComponent, ProductTagComponent],
  imports: [
    CommonModule,
    TagRoutingModule,
    CoreModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    ColorSketchModule,
    NgSelectModule
  ]
})
export class TagModule { }
