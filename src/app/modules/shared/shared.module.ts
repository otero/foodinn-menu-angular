import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LayoutMainComponent } from './components/layout-main/layout-main.component';
import { LayoutAdminMainComponent } from './components/layout-admin-main/layout-admin-main.component';
import { CoreModule } from '../core/core.module';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { MaterialModule } from '../material/material.module';
import { LayoutAdminAuthComponent } from './components/layout-admin-auth/layout-admin-auth.component';
import { TranslateModule } from '@ngx-translate/core';
import { DialogComponent } from './components/dialog/dialog.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ShowComponent } from './components/file-upload/components/show/show.component';
import { AdComponent } from './components/ad/ad.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    LayoutMainComponent,
    LayoutAdminMainComponent,
    LayoutAdminAuthComponent,
    DialogComponent,
    FileUploadComponent,
    ShowComponent,
    AdComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    CoreModule,
    MaterialModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    LoadingBarModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  exports:  [
    FileUploadComponent,
    TranslateModule,
    HeaderComponent,
    FooterComponent
  ]
})
export class SharedModule { }
