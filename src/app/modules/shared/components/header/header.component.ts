import { Component, OnInit, Input} from '@angular/core';
import { CommonService } from 'src/app/modules/core/services/common/common.service';

import { Customer } from '../../../core/models/customer';
import { User } from '../../../core/models/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() taxonomies: any[];
  @Input() menus: any[];
  @Input() customer: Customer;
  @Input() user: User;
  @Input() nav: Array<any>;

  isOpenLang: boolean = false;
  defaultLang: string;

  constructor(
    private commonService: CommonService
  ) {
    this.defaultLang = this.commonService.defaultLang;
  }

  ngOnInit(): void {
  }

  openLang(event): void {
    event.preventDefault();

    this.isOpenLang = true;
  }

  changeLang(lang): void {
    this.isOpenLang = false;
    this.commonService.changeLang(lang);
    this.defaultLang = lang;
  }
}
