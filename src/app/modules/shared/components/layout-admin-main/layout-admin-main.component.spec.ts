import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutAdminMainComponent } from './layout-admin-main.component';

describe('LayoutAdminMainComponent', () => {
  let component: LayoutAdminMainComponent;
  let fixture: ComponentFixture<LayoutAdminMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutAdminMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutAdminMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
