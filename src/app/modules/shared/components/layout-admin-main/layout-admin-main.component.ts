import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar } from '@angular/material/snack-bar';

import { Observable } from 'rxjs';

import { AuthService } from 'src/app/modules/core/services/auth/auth.service';
import { ToastService } from 'src/app/modules/core/services/toast/toast.service';

@Component({
  selector: 'app-layout-admin-main',
  templateUrl: './layout-admin-main.component.html',
  styleUrls: ['./layout-admin-main.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class LayoutAdminMainComponent implements OnInit {

  toast$: Observable<any>;

  constructor(
    private authService: AuthService,
    private toastService: ToastService,
    private translateService: TranslateService,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit(): void{
    this.toastService.toast$.subscribe(
      response => {
        this.snackBar.open(
          this.translateService.instant(response.messageTranslate),
          this.translateService.instant('COMMON.CLOSE'),
          {
            duration: 5000,
            verticalPosition: response.verticalPosition
          }
        );
      }
    );
  }

  logout(event): void {
    event.preventDefault();
    this.authService.deleteToken();
    this.router.navigate(['admin/auth/login']);
    window.location.reload();
  }
}
