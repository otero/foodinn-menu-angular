import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-layout-admin-auth',
  templateUrl: './layout-admin-auth.component.html',
  styleUrls: ['./layout-admin-auth.component.css']
})
export class LayoutAdminAuthComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
}
