import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { UtilService } from 'src/app/modules/core/services/util/util.service';
import { ShowComponent } from './components/show/show.component';


@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  @Input() requiredFileType:any;
  @Input() type: string;
  @Input() file: string = '';
  @Output() emit = new EventEmitter<any>();

  types: any = {
    'image': [
      'image/png',
      'image/jpeg',
      'image/jpg',
      'image/gif'
    ]
  };
  fileName = '';
  uploadProgress:number;
  uploadSub: Subscription;

  constructor(private utilService: UtilService, public dialog: MatDialog) {}

  ngOnInit() {}

  onFileSelected(event) {
    const file:File = event.target.files[0];
    this.types['image'].find(
      rol => {
        if (rol == file.type) {
          this.type = 'image';
        }
      }
    );

    if (file) {
      this.fileName = file.name;

      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        const upload$=  this.utilService.upload(reader.result).pipe(
            finalize(() => this.reset())
        );

        this.uploadSub = upload$.subscribe(event => {
          if (event.type == HttpEventType.UploadProgress) {
            this.uploadProgress = Math.round(100 * (event.loaded / event.total));
          }
          if (event instanceof HttpResponse) {
            if (event.body != undefined) {
              this.file = event.body.data.image_url;
              this.doEmit();
            }
          }
        },
        () => {
        });
      }
    }
  }

  doEmit(): void {
    this.emit.emit({
      file: this.file,
      type: this.type
    });
  }

  openShow(event): void {
    event.preventDefault();
    this.dialog.open(ShowComponent, {
      width: '600px',
      data: {
       file: this.file,
       type: this.type
      }
    });
  }

  cancelUpload() {
    this.uploadSub.unsubscribe();
    this.reset();
  }

  reset() {
    this.uploadProgress = null;
    this.uploadSub = null;
  }
}
