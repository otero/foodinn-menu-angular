import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Customer } from 'src/app/modules/core/models/customer';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { CustomerService } from 'src/app/modules/core/services/customer/customer.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  taxonomy: any;
  filters: string = 'limit=100000'
  filtersAny: string = `status[eq]=active&${this.filters}`;
  customer: Customer;

  constructor(
    private taxonomyService: TaxonomyService,
    private customerService: CustomerService,
  ) {
    this.customer = this.customerService.loggedCustomer;
  }

  ngOnInit(): void {
    this.taxonomyService.getAll(
      `${this.filtersAny}&visible[eq]=1&promoted[eq]=1`
    )
    .pipe(
      map(taxonomies => {
        return taxonomies[0];
    }))
    .subscribe(
      taxonomy => {
        this.taxonomy = taxonomy.id;
      },
      () => {
        this.taxonomy = null;
      }
    );
  }

}
