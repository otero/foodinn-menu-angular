import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Ad } from 'src/app/modules/core/models/ad';
import { Product } from 'src/app/modules/core/models/product';
import { Taxonomy } from 'src/app/modules/core/models/taxonomy';
import { AdService } from 'src/app/modules/core/services/ad/ad.service';
import { CommonService } from 'src/app/modules/core/services/common/common.service';
import { ProductPriceListService } from 'src/app/modules/core/services/product-price-list/product-price-list.service';
import { ProductService } from 'src/app/modules/core/services/product/product.service';
import { SalesCenterService } from 'src/app/modules/core/services/sales-center/sales-center.service';
import { TaxonomyService } from 'src/app/modules/core/services/taxonomy/taxonomy.service';

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {

  ads: Ad[] = [];
  defaultLang: string;

  filters: string = 'limit=100000'
  filtersAny: string = `status[eq]=active&${this.filters}`;
  ad: any;
  showAd: boolean = false;
  globalAd: boolean;
  subscribers = [];
  products: Product[] = [];
  taxonomies: Taxonomy[] = [];
  vertical = '0px';

  constructor(
    private commonService: CommonService,
    private adService: AdService,
    private productPriceListService: ProductPriceListService,
    private salesCenterService: SalesCenterService,
    private productService: ProductService,
    private taxonomyService: TaxonomyService
  ) {
    this.defaultLang = this.commonService.defaultLang;

  }

  ngOnInit(): void {
    this.fetchAds();

    this.vertical =  `${window.innerHeight - 150}px`;
  }

  fetchAds(): void {
    this.adService.getAll(`${this.filtersAny}&visible[eq]=1`).subscribe(
      ads => {
        this.ads = ads;
        this.setAds();
      },
      () => {
        this.ads = [];
      }
    );
  }

  setAds(): void {
    for (let ad of this.ads) {
      let adSubscriber = setTimeout(() => {
        this.ad = null;
        this.ad = {
          type: ad.type,
          image_url: ad.image_url,
          translations: ad.translations,
          product_lists: ad.products_list,
          taxonomy_lists: ad.taxonomies_list,
          external_link: ad.external_link,
          only_one_product: (
            ad.products_list != null &&
            ad.products_list.length == 1 && ad.type == 'image'
            ) ? true : false,
          only_one_taxonomy: (
            ad.taxonomies_list != null &&
            ad.taxonomies_list.length == 1 && ad.type == 'image'
            ) ? true : false
        }

        this.fetchProducts(ad.products_list);
        this.fetchTaxonomies(ad.taxonomies_list);

        this.showAd = true;
        this.globalAd = this.commonService.ad;

      }, ad.show_on_minute * 60000);

      this.subscribers.push(adSubscriber);
    }
  }

  private fetchProducts(product_lists): void {
    if (product_lists == null) {
      this.products = [];
      return;
    }

    for (let product of product_lists) {
      this.productService.get(product)
      .subscribe(
        product => {
          this.fetchProductPriceList(product);
          this.products.push(product);
        },
        () => {}
      );
    }
  }

  private fetchTaxonomies(taxonomies_lists): void {
    if (taxonomies_lists == null) {
      this.taxonomies = [];
      return;
    }

    for (let taxonomy of taxonomies_lists) {
      this.taxonomyService.get(taxonomy)
      .subscribe(
        taxonomy => {
          this.taxonomies.push(taxonomy);
        },
        () => {}
      );
    }
  }

  private fetchProductPriceList(product): void {
    this.productPriceListService
      .getAll(
        `${this.filtersAny}&product[eq]=${product.id}&priceList[eq]=${this.salesCenterService.loggedSalesCenter.price_list.id}`
      )
      .pipe(map(productsPriceList => {
        return productsPriceList[0]
      }))
      .subscribe(
        productPriceList => {
          product.price = productPriceList.main_price;
          product.product_price_list = productPriceList;
        },
        () => {}
      );
  }

  close(event): void {
    this.showAd = false;
  }
}
