import { Component } from '@angular/core';

import { LocalizationService } from '../../../core/services/localization/localization.service';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-localization',
  templateUrl: './localization.component.html',
  styleUrls: ['./localization.component.css']
})
export class LocalizationComponent{

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private localizationService: LocalizationService
  ) {
    combineLatest( [this.route.paramMap, this.route.queryParamMap] )
      .subscribe( ([pathParams, queryParams]) => {
        const reference = pathParams.get('reference');
        const name    = queryParams.get('name');
        const id = pathParams.get('id');
        this.localizationService.generateReference(reference, id, name);
      });
    this.router.navigate([
      '/home'
    ]);
  }
}
