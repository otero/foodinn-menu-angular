import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocalizationComponent } from './components/localization/localization.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { InactiveComponent } from './components/inactive/inactive.component';

const routes: Routes = [
  { path: 'add/:reference/:id', component: LocalizationComponent, data: {title: 'Localization Add'}, },
  { path: 'error', component: NotFoundComponent, data: {title: 'Localization Error'}, },
  { path: 'inactive', component: InactiveComponent, data: {title: 'Localization Inactive'} }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocalizationRoutingModule { }
