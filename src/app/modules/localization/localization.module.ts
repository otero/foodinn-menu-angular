import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalizationRoutingModule } from './localization-routing.module';
import { LocalizationComponent } from './components/localization/localization.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { InactiveComponent } from './components/inactive/inactive.component';
import { CoreModule } from '../core/core.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [LocalizationComponent, NotFoundComponent, InactiveComponent],
  imports: [
    CommonModule,
    LocalizationRoutingModule,
    CoreModule,
    SharedModule
  ]
})
export class LocalizationModule { }
