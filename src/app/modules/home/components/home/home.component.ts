import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../core/services/auth/auth.service';
import { UserService } from '../../../core/services/user/user.service';
import { User } from '../../../core/models/user';
import { LocalizationService } from '../../../core/services/localization/localization.service';
import { Localization } from '../../../core/models/localization';
import { CommonService } from 'src/app/modules/core/services/common/common.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: any;
  localization: Localization;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private localizationService: LocalizationService,
    private commonService: CommonService,
    private router: Router
  ) {
    this.localization = this.localizationService.localization;
    this.create();
  }

  ngOnInit(): void {
  }

  create(): void {
    this.userService.generateUsernameAndPassword();
    this.user = {
      uuid: localStorage.getItem('_username'),
      plain_password: localStorage.getItem('_username'),
      full_name: localStorage.getItem('_username'),
      lang: this.commonService.defaultLang,
      location: parseFloat(this.localizationService.getLocalization.location)
    };
    this.userService.create(this.user).subscribe(
      () => {
        this.login(this.user.uuid, this.user.plain_password);
      }
    );

  }

  private login(uuid, password): void{
    this.authService.login({
      _username: uuid,
      _password: password
    })
      .then(
      () => {
        this.router.navigate(['products']);
      }
    )
    .catch(() => {
      this.authService.deleteToken();
    });
  }
}
