import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FeedbackService } from '../../../core/services/feedback/feedback.service';
import { LocationService } from 'src/app/modules/core/services/location/location.service';
import { Location } from 'src/app/modules/core/models/location';
import { CustomerService } from 'src/app/modules/core/services/customer/customer.service';
import { Customer } from 'src/app/modules/core/models/customer';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  location: Location;
  form: FormGroup;
  rating = 5;
  customer: Customer;
  response: boolean = false;

  constructor(
    private feedbackService: FeedbackService,
    private locationService: LocationService,
    private customerService: CustomerService,
    private formBuilder: FormBuilder
  ) {
    this.customer = this.customerService.loggedCustomer;
    this.location = this.locationService.loggedLocation;
    this.buildForm();
  }

  ngOnInit(): void {

  }

  send(event): void {
    event.preventDefault();
    if (this.form.valid) {
      this.feedbackService.create(
        this.mapDTO(this.form.value)
      ).subscribe(
        () => {
          this.response = true;
        },
        () => {
          this.response = false;
        }
      );
    }
  }

  private mapDTO(form) {
    const DTO = {...form}

    if (DTO.comment == null) {
      delete DTO.comment;
    }

    return form;
  }


  changeRating(event, rating): void {
    $('.btn-rating').removeClass('active');
    $(event.target).addClass('active');
    $(event.target).parent().addClass('active');
    this.form.get('rating').patchValue(rating);
  }

  private buildForm(): void{
    this.form = this.formBuilder.group({
      location: this.locationService.loggedLocation.id,
      rating: [null, [Validators.required]],
      comment: ['', []]
    });
  }
}
