
export const environment = {
  production: false,
  apiURL: 'https://pre.menuqrfoodinnappapi.com/api',
  webAppURL: 'localhost:4200',
  company: 'Foodinn',
  redsysURL: 'https://sis-t.redsys.es:25443/sis/realizarPago',
  S3bucket: 'https://foodinnapp-pre.s3.eu-west-3.amazonaws.com',
  GTMCode: 'G-ERNN6E05G4',
  onSignalAppId: '9ddb3eae-0fcb-452c-a0b0-c2add9d8195e',
  oneSinalSafariWebId: 'web.onesignal.auto.5a2165c8-9d94-4308-bfd9-99a8484077b6'
};
