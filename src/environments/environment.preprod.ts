
export const environment = {
  production: true,
  apiURL: 'https://pre.menuqrfoodinnappapi.com/api',
  webAppURL: 'https://app.pre.menuqrfoodinnapp.com',
  company: 'Foodinn',
  redsysURL: 'https://sis-t.redsys.es:25443/sis/realizarPago',
  S3bucket: 'https://foodinnapp-pre.s3.eu-west-3.amazonaws.com',
  GTMCode: 'G-ERNN6E05G4',
  onSignalAppId: '3bd106c5-2ef5-4769-aa79-83556733a0fa',
  oneSinalSafariWebId: 'web.onesignal.auto.3c5e9739-5d2f-4f69-94b2-78aed3043174'
};
