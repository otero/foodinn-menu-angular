
export const environment = {
  production: true,
  apiURL: 'https://prod.menuqrfoodinnappapi.com/api',
  webAppURL: 'https://app.menuqrfoodinnapp.com',
  company: 'Foodinn',
  redsysURL: 'https://sis-t.redsys.es:25443/sis/realizarPago',
  S3bucket: 'https://foodinnapp-pre.s3.eu-west-3.amazonaws.com',
  GTMCode: 'G-ERNN6E05G4',
  onSignalAppId: '3ba3ceef-9036-4a0c-a3ad-916bbf971b82',
  oneSinalSafariWebId: 'web.onesignal.auto.1d9d9717-02c9-46fa-a0ca-aedc9bb61733'
};
